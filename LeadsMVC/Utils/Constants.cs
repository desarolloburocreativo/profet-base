﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeadsMVC.Utils
{
    public static class Constants
    {
        public static class Sort
        {
            public const string ASC = "ASC";
            public const string DESC = "DESC";
        }

        public static class Codes
        {
            public const int PutNotes = 7624;
            public const int MyCampaigns = 47765;
        }

        public static class Segmentation
        {
            public const string All = "ALL_LEADS";
            public const string Own = "OWN_LEADS";
            public const string Pool = "POOL_LEADS";
        }
    }
}
