namespace LeadsMVC.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addedCalledField : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Leads", "Called", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Leads", "Called");
        }
    }
}
