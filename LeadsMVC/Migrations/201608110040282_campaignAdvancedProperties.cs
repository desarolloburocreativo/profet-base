namespace LeadsMVC.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class campaignAdvancedProperties : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Campaigns", "Impressions", c => c.Double(nullable: false));
            AddColumn("dbo.Campaigns", "Clicks", c => c.Double(nullable: false));
            AddColumn("dbo.Campaigns", "Engagement", c => c.Double(nullable: false));
            AddColumn("dbo.Campaigns", "Community", c => c.Double(nullable: false));
            AddColumn("dbo.Campaigns", "AdminBudget", c => c.Double(nullable: false));
            AddColumn("dbo.Campaigns", "MediaBudget", c => c.Double(nullable: false));
            AddColumn("dbo.Campaigns", "SalesRep", c => c.String());
            AddColumn("dbo.Campaigns", "AccountMgmt", c => c.String());
            AddColumn("dbo.Campaigns", "ContractUrl", c => c.String());
            AddColumn("dbo.Campaigns", "ConfigurationUrl", c => c.String());
            AddColumn("dbo.Campaigns", "StatmentUrl", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Campaigns", "StatmentUrl");
            DropColumn("dbo.Campaigns", "ConfigurationUrl");
            DropColumn("dbo.Campaigns", "ContractUrl");
            DropColumn("dbo.Campaigns", "AccountMgmt");
            DropColumn("dbo.Campaigns", "SalesRep");
            DropColumn("dbo.Campaigns", "MediaBudget");
            DropColumn("dbo.Campaigns", "AdminBudget");
            DropColumn("dbo.Campaigns", "Community");
            DropColumn("dbo.Campaigns", "Engagement");
            DropColumn("dbo.Campaigns", "Clicks");
            DropColumn("dbo.Campaigns", "Impressions");
        }
    }
}
