namespace LeadsMVC.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ReportsFeatures : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Reports", "Url", c => c.String());
            AddColumn("dbo.Reports", "UploadedDate", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Reports", "UploadedDate");
            DropColumn("dbo.Reports", "Url");
        }
    }
}
