namespace LeadsMVC.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ReportsFixes : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Reports", "Url");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Reports", "Url", c => c.String());
        }
    }
}
