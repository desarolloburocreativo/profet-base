namespace LeadsMVC.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedCampaign : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Campaigns",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Leads", "CampaignName", c => c.String());
            AddColumn("dbo.Leads", "Campaign_Id", c => c.Long());
            CreateIndex("dbo.Leads", "Campaign_Id");
            AddForeignKey("dbo.Leads", "Campaign_Id", "dbo.Campaigns", "Id");
            DropColumn("dbo.Leads", "Campaign");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Leads", "Campaign", c => c.String());
            DropForeignKey("dbo.Leads", "Campaign_Id", "dbo.Campaigns");
            DropIndex("dbo.Leads", new[] { "Campaign_Id" });
            DropColumn("dbo.Leads", "Campaign_Id");
            DropColumn("dbo.Leads", "CampaignName");
            DropTable("dbo.Campaigns");
        }
    }
}
