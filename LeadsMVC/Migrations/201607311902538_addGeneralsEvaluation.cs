namespace LeadsMVC.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addGeneralsEvaluation : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Leads", "Generals", c => c.Double());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Leads", "Generals");
        }
    }
}
