namespace LeadsMVC.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FixNullDates : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Leads", "LeadDate", c => c.DateTime());
            AlterColumn("dbo.Leads", "LeadHour", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Leads", "LeadHour", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Leads", "LeadDate", c => c.DateTime(nullable: false));
        }
    }
}
