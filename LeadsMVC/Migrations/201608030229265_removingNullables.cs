namespace LeadsMVC.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class removingNullables : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Leads", "Employees", c => c.Int(nullable: false));
            AlterColumn("dbo.Leads", "LeadDate", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Leads", "SellApproxAmount", c => c.Double(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Leads", "SellApproxAmount", c => c.Double());
            AlterColumn("dbo.Leads", "LeadDate", c => c.DateTime());
            AlterColumn("dbo.Leads", "Employees", c => c.Int());
        }
    }
}
