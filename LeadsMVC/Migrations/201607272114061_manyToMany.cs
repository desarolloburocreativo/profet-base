namespace LeadsMVC.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class manyToMany : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Industries",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Name = c.String(),
                        Letter = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.UserIndusty",
                c => new
                    {
                        ApplicationUser_Id = c.String(nullable: false, maxLength: 128),
                        Industry_Id = c.Long(nullable: false),
                    })
                .PrimaryKey(t => new { t.ApplicationUser_Id, t.Industry_Id })
                .ForeignKey("dbo.Users", t => t.ApplicationUser_Id, cascadeDelete: true)
                .ForeignKey("dbo.Industries", t => t.Industry_Id, cascadeDelete: true)
                .Index(t => t.ApplicationUser_Id)
                .Index(t => t.Industry_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UserIndusty", "Industry_Id", "dbo.Industries");
            DropForeignKey("dbo.UserIndusty", "ApplicationUser_Id", "dbo.Users");
            DropIndex("dbo.UserIndusty", new[] { "Industry_Id" });
            DropIndex("dbo.UserIndusty", new[] { "ApplicationUser_Id" });
            DropTable("dbo.UserIndusty");
            DropTable("dbo.Industries");
        }
    }
}
