namespace LeadsMVC.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class nonNullUpsellPotential : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Leads", "UpsellPotential", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Leads", "UpsellPotential", c => c.Boolean());
        }
    }
}
