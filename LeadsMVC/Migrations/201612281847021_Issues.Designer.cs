// <auto-generated />
namespace LeadsMVC.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class Issues : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(Issues));
        
        string IMigrationMetadata.Id
        {
            get { return "201612281847021_Issues"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return Resources.GetString("Source"); }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
