// <auto-generated />
namespace LeadsMVC.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class removingNullables : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(removingNullables));
        
        string IMigrationMetadata.Id
        {
            get { return "201608030229265_removingNullables"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
