namespace LeadsMVC.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class industryDescription : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Industries", "Description", c => c.String());
            DropColumn("dbo.Industries", "Letter");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Industries", "Letter", c => c.String());
            DropColumn("dbo.Industries", "Description");
        }
    }
}
