namespace LeadsMVC.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UserSector : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Users", "IndustrySector", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Users", "IndustrySector");
        }
    }
}
