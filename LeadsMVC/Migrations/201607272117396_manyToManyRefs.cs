namespace LeadsMVC.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class manyToManyRefs : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.UserIndusty", name: "ApplicationUser_Id", newName: "UserId");
            RenameColumn(table: "dbo.UserIndusty", name: "Industry_Id", newName: "IndustryId");
            RenameIndex(table: "dbo.UserIndusty", name: "IX_ApplicationUser_Id", newName: "IX_UserId");
            RenameIndex(table: "dbo.UserIndusty", name: "IX_Industry_Id", newName: "IX_IndustryId");
        }
        
        public override void Down()
        {
            RenameIndex(table: "dbo.UserIndusty", name: "IX_IndustryId", newName: "IX_Industry_Id");
            RenameIndex(table: "dbo.UserIndusty", name: "IX_UserId", newName: "IX_ApplicationUser_Id");
            RenameColumn(table: "dbo.UserIndusty", name: "IndustryId", newName: "Industry_Id");
            RenameColumn(table: "dbo.UserIndusty", name: "UserId", newName: "ApplicationUser_Id");
        }
    }
}
