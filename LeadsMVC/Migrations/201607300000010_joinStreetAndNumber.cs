namespace LeadsMVC.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class joinStreetAndNumber : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Leads", "StreetAndNumber", c => c.String());
            DropColumn("dbo.Leads", "Street");
            DropColumn("dbo.Leads", "Number");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Leads", "Number", c => c.String());
            AddColumn("dbo.Leads", "Street", c => c.String());
            DropColumn("dbo.Leads", "StreetAndNumber");
        }
    }
}
