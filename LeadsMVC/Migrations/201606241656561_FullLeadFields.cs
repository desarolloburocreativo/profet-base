namespace LeadsMVC.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FullLeadFields : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Leads", "Genre", c => c.String());
            AddColumn("dbo.Leads", "CivilStatus", c => c.String());
            AddColumn("dbo.Leads", "Age", c => c.String());
            AddColumn("dbo.Leads", "Birthday", c => c.String());
            AddColumn("dbo.Leads", "StreetAndNumber", c => c.String());
            AddColumn("dbo.Leads", "Colony", c => c.String());
            AddColumn("dbo.Leads", "City", c => c.String());
            AddColumn("dbo.Leads", "State", c => c.String());
            AddColumn("dbo.Leads", "CP", c => c.String());
            AddColumn("dbo.Leads", "NSE", c => c.String());
            AddColumn("dbo.Leads", "Position", c => c.String());
            AddColumn("dbo.Leads", "Company", c => c.String());
            AddColumn("dbo.Leads", "CompanyType", c => c.String());
            AddColumn("dbo.Leads", "IndistrySector", c => c.String());
            AddColumn("dbo.Leads", "Employees", c => c.Int(nullable: true));
            AddColumn("dbo.Leads", "Interests", c => c.String());
            AddColumn("dbo.Leads", "Preferences", c => c.String());
            AddColumn("dbo.Leads", "Activities", c => c.String());
            AddColumn("dbo.Leads", "PracticeSports", c => c.String());
            AddColumn("dbo.Leads", "Habits", c => c.String());
            AddColumn("dbo.Leads", "Values", c => c.String());
            AddColumn("dbo.Leads", "Believes", c => c.String());
            AddColumn("dbo.Leads", "Emotions", c => c.String());
            AddColumn("dbo.Leads", "BehaviorTags", c => c.String());
            AddColumn("dbo.Leads", "BuyingTime", c => c.String());
            AddColumn("dbo.Leads", "BuyingDecision", c => c.String());
            AddColumn("dbo.Leads", "NeedDegree", c => c.String());
            AddColumn("dbo.Leads", "ComparingAgainst", c => c.String());
            AddColumn("dbo.Leads", "AuthorityTags", c => c.String());
            AddColumn("dbo.Leads", "LeadDate", c => c.DateTime(nullable: true));
            AddColumn("dbo.Leads", "LeadHour", c => c.DateTime(nullable: true));
            AddColumn("dbo.Leads", "InterestedIn", c => c.String());
            AddColumn("dbo.Leads", "MessageSent", c => c.String());
            AddColumn("dbo.Leads", "OpportunityValue", c => c.String());
            AddColumn("dbo.Leads", "ProspectSource", c => c.String());
            AddColumn("dbo.Leads", "ProsepectGeneratorCompany", c => c.String());
            AddColumn("dbo.Leads", "Campaign", c => c.String());
            AddColumn("dbo.Leads", "InterestTags", c => c.String());
            AddColumn("dbo.Leads", "Budget", c => c.Double(nullable: true));
            AddColumn("dbo.Leads", "Job", c => c.Double(nullable: true));
            AddColumn("dbo.Leads", "Authority", c => c.Double(nullable: true));
            AddColumn("dbo.Leads", "Need", c => c.Double(nullable: true));
            AddColumn("dbo.Leads", "TimeFrame", c => c.Double(nullable: true));
            AddColumn("dbo.Leads", "Type", c => c.Double(nullable: true));
            AddColumn("dbo.Leads", "Industry", c => c.Double(nullable: true));
            AddColumn("dbo.Leads", "Status", c => c.Double(nullable: true));
            AddColumn("dbo.Leads", "Contact", c => c.Double(nullable: true));
            AddColumn("dbo.Leads", "QualityScore", c => c.Double(nullable: true));
            AddColumn("dbo.Leads", "Aging", c => c.Double(nullable: true));
            AddColumn("dbo.Leads", "Engagement", c => c.Double(nullable: true));
            AddColumn("dbo.Leads", "UpsellPotential", c => c.Double(nullable: true));
            AddColumn("dbo.Leads", "LeadScore", c => c.Double(nullable: true));
            AddColumn("dbo.Leads", "ClosedSell", c => c.String());
            AddColumn("dbo.Leads", "SellApproxAmount", c => c.Double(nullable: true));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Leads", "SellApproxAmount");
            DropColumn("dbo.Leads", "ClosedSell");
            DropColumn("dbo.Leads", "LeadScore");
            DropColumn("dbo.Leads", "UpsellPotential");
            DropColumn("dbo.Leads", "Engagement");
            DropColumn("dbo.Leads", "Aging");
            DropColumn("dbo.Leads", "QualityScore");
            DropColumn("dbo.Leads", "Contact");
            DropColumn("dbo.Leads", "Status");
            DropColumn("dbo.Leads", "Industry");
            DropColumn("dbo.Leads", "Type");
            DropColumn("dbo.Leads", "TimeFrame");
            DropColumn("dbo.Leads", "Need");
            DropColumn("dbo.Leads", "Authority");
            DropColumn("dbo.Leads", "Job");
            DropColumn("dbo.Leads", "Budget");
            DropColumn("dbo.Leads", "InterestTags");
            DropColumn("dbo.Leads", "Campaign");
            DropColumn("dbo.Leads", "ProsepectGeneratorCompany");
            DropColumn("dbo.Leads", "ProspectSource");
            DropColumn("dbo.Leads", "OpportunityValue");
            DropColumn("dbo.Leads", "MessageSent");
            DropColumn("dbo.Leads", "InterestedIn");
            DropColumn("dbo.Leads", "LeadHour");
            DropColumn("dbo.Leads", "LeadDate");
            DropColumn("dbo.Leads", "AuthorityTags");
            DropColumn("dbo.Leads", "ComparingAgainst");
            DropColumn("dbo.Leads", "NeedDegree");
            DropColumn("dbo.Leads", "BuyingDecision");
            DropColumn("dbo.Leads", "BuyingTime");
            DropColumn("dbo.Leads", "BehaviorTags");
            DropColumn("dbo.Leads", "Emotions");
            DropColumn("dbo.Leads", "Believes");
            DropColumn("dbo.Leads", "Values");
            DropColumn("dbo.Leads", "Habits");
            DropColumn("dbo.Leads", "PracticeSports");
            DropColumn("dbo.Leads", "Activities");
            DropColumn("dbo.Leads", "Preferences");
            DropColumn("dbo.Leads", "Interests");
            DropColumn("dbo.Leads", "Employees");
            DropColumn("dbo.Leads", "IndistrySector");
            DropColumn("dbo.Leads", "CompanyType");
            DropColumn("dbo.Leads", "Company");
            DropColumn("dbo.Leads", "Position");
            DropColumn("dbo.Leads", "NSE");
            DropColumn("dbo.Leads", "CP");
            DropColumn("dbo.Leads", "State");
            DropColumn("dbo.Leads", "City");
            DropColumn("dbo.Leads", "Colony");
            DropColumn("dbo.Leads", "StreetAndNumber");
            DropColumn("dbo.Leads", "Birthday");
            DropColumn("dbo.Leads", "Age");
            DropColumn("dbo.Leads", "CivilStatus");
            DropColumn("dbo.Leads", "Genre");
        }
    }
}
