namespace LeadsMVC.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class buyingAuthorityMod : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Leads", "BuyingTime", c => c.Double());
            AlterColumn("dbo.Leads", "BuyingDecision", c => c.Double());
            AlterColumn("dbo.Leads", "NeedDegree", c => c.Double());
            AlterColumn("dbo.Leads", "ComparingAgainst", c => c.Double());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Leads", "ComparingAgainst", c => c.String());
            AlterColumn("dbo.Leads", "NeedDegree", c => c.String());
            AlterColumn("dbo.Leads", "BuyingDecision", c => c.String());
            AlterColumn("dbo.Leads", "BuyingTime", c => c.String());
        }
    }
}
