namespace LeadsMVC.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changeNullableBools : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Leads", "BuyingTime", c => c.Double(nullable: false, defaultValue: 0));
            AlterColumn("dbo.Leads", "BuyingDecision", c => c.Double(nullable: false, defaultValue: 0));
            AlterColumn("dbo.Leads", "NeedDegree", c => c.Double(nullable: false, defaultValue: 0));
            AlterColumn("dbo.Leads", "ComparingAgainst", c => c.Double(nullable: false, defaultValue: 0));
            AlterColumn("dbo.Leads", "Generals", c => c.Double(nullable: false, defaultValue: 0));
            AlterColumn("dbo.Leads", "Budget", c => c.Double(nullable: false, defaultValue: 0));
            AlterColumn("dbo.Leads", "Job", c => c.Double(nullable: false, defaultValue: 0));
            AlterColumn("dbo.Leads", "Authority", c => c.Double(nullable: false, defaultValue: 0));
            AlterColumn("dbo.Leads", "Need", c => c.Double(nullable: false, defaultValue: 0));
            AlterColumn("dbo.Leads", "TimeFrame", c => c.Double(nullable: false, defaultValue: 0));
            AlterColumn("dbo.Leads", "Type", c => c.Double(nullable: false, defaultValue: 0));
            AlterColumn("dbo.Leads", "Industry", c => c.Double(nullable: false, defaultValue: 0));
            AlterColumn("dbo.Leads", "Status", c => c.Double(nullable: false, defaultValue: 0));
            AlterColumn("dbo.Leads", "Contact", c => c.Double(nullable: false, defaultValue: 0));
            AlterColumn("dbo.Leads", "QualityScore", c => c.Double(nullable: false, defaultValue: 0));
            AlterColumn("dbo.Leads", "Aging", c => c.Double(nullable: false, defaultValue: 0));
            AlterColumn("dbo.Leads", "Engagement", c => c.Double(nullable: false, defaultValue: 0));
            AlterColumn("dbo.Leads", "LeadScore", c => c.Double(nullable: false, defaultValue: 0));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Leads", "LeadScore", c => c.Double());
            AlterColumn("dbo.Leads", "Engagement", c => c.Double());
            AlterColumn("dbo.Leads", "Aging", c => c.Double());
            AlterColumn("dbo.Leads", "QualityScore", c => c.Double());
            AlterColumn("dbo.Leads", "Contact", c => c.Double());
            AlterColumn("dbo.Leads", "Status", c => c.Double());
            AlterColumn("dbo.Leads", "Industry", c => c.Double());
            AlterColumn("dbo.Leads", "Type", c => c.Double());
            AlterColumn("dbo.Leads", "TimeFrame", c => c.Double());
            AlterColumn("dbo.Leads", "Need", c => c.Double());
            AlterColumn("dbo.Leads", "Authority", c => c.Double());
            AlterColumn("dbo.Leads", "Job", c => c.Double());
            AlterColumn("dbo.Leads", "Budget", c => c.Double());
            AlterColumn("dbo.Leads", "Generals", c => c.Double());
            AlterColumn("dbo.Leads", "ComparingAgainst", c => c.Double());
            AlterColumn("dbo.Leads", "NeedDegree", c => c.Double());
            AlterColumn("dbo.Leads", "BuyingDecision", c => c.Double());
            AlterColumn("dbo.Leads", "BuyingTime", c => c.Double());
        }
    }
}
