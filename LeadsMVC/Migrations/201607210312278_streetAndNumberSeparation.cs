namespace LeadsMVC.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class streetAndNumberSeparation : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Leads", "Street", c => c.String());
            AddColumn("dbo.Leads", "Number", c => c.String());
            DropColumn("dbo.Leads", "StreetAndNumber");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Leads", "StreetAndNumber", c => c.String());
            DropColumn("dbo.Leads", "Number");
            DropColumn("dbo.Leads", "Street");
        }
    }
}
