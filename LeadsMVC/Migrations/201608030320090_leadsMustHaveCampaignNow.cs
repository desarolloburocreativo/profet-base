namespace LeadsMVC.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class leadsMustHaveCampaignNow : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Leads", "CampaignId", "dbo.Campaigns");
            DropIndex("dbo.Leads", new[] { "CampaignId" });
            AlterColumn("dbo.Leads", "CampaignId", c => c.Long(nullable: false));
            CreateIndex("dbo.Leads", "CampaignId");
            AddForeignKey("dbo.Leads", "CampaignId", "dbo.Campaigns", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Leads", "CampaignId", "dbo.Campaigns");
            DropIndex("dbo.Leads", new[] { "CampaignId" });
            AlterColumn("dbo.Leads", "CampaignId", c => c.Long());
            CreateIndex("dbo.Leads", "CampaignId");
            AddForeignKey("dbo.Leads", "CampaignId", "dbo.Campaigns", "Id");
        }
    }
}
