namespace LeadsMVC.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class userExtendedProperties : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Users", "CompanyName", c => c.String());
            AddColumn("dbo.Users", "Phone", c => c.String());
            AddColumn("dbo.Users", "Address", c => c.String());
            AddColumn("dbo.Users", "Web", c => c.String());
            AddColumn("dbo.Users", "Contact", c => c.String());
            AddColumn("dbo.Users", "PhoneExt", c => c.String());
            AddColumn("dbo.Users", "Mobile", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Users", "Mobile");
            DropColumn("dbo.Users", "PhoneExt");
            DropColumn("dbo.Users", "Contact");
            DropColumn("dbo.Users", "Web");
            DropColumn("dbo.Users", "Address");
            DropColumn("dbo.Users", "Phone");
            DropColumn("dbo.Users", "CompanyName");
        }
    }
}
