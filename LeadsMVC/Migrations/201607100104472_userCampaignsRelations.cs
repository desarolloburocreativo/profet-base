namespace LeadsMVC.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class userCampaignsRelations : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Campaigns", "User_Id", c => c.String(maxLength: 128));
            CreateIndex("dbo.Campaigns", "User_Id");
            AddForeignKey("dbo.Campaigns", "User_Id", "dbo.Users", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Campaigns", "User_Id", "dbo.Users");
            DropIndex("dbo.Campaigns", new[] { "User_Id" });
            DropColumn("dbo.Campaigns", "User_Id");
        }
    }
}
