namespace LeadsMVC.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddNotesRemoveLeadHour : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Leads", "Notes", c => c.String());
            DropColumn("dbo.Leads", "LeadHour");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Leads", "LeadHour", c => c.DateTime());
            DropColumn("dbo.Leads", "Notes");
        }
    }
}
