namespace LeadsMVC.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addedCampaignServiceCost : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Campaigns", "ServiceCost", c => c.Double(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Campaigns", "ServiceCost");
        }
    }
}
