﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeadsMVC.Models
{
    public class Campaign
    {
        [Key]
        public long Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        // Campaign advanced properties
        // ===========================================================
        public double ServiceCost { get; set; }

        // KPIS
        public double Impressions { get; set; }
        public double? LeadsCount { get; set; }
        public double Clicks { get; set; }
        public double Engagement { get; set; }
        public double Community { get; set; }

        // Budget
        public double AdminBudget { get; set; }
        public double MediaBudget { get; set; }

        // Contacts
        public string SalesRep { get; set; }
        public string AccountMgmt { get; set; }

        // Documentaion
        public string ContractUrl { get; set; }
        public string ConfigurationUrl { get; set; }
        public string StatmentUrl { get; set; }
             
        // ===========================================================

        // Relation one-to-many
        [JsonIgnore]
        public virtual ICollection<Lead> Leads { get; set; }
        public Campaign() { Leads = new List<Lead>(); }

        // Relation for users
        [JsonIgnore]
        [ForeignKey("UserId")]
        public virtual ApplicationUser User { get; set; }
        [JsonIgnore]
        public string UserId { get; set; }

        public static Campaign CreateSelectAll()
        {
            return new Campaign() { Id = -1, Name = "Todas" };
        }
    }
}