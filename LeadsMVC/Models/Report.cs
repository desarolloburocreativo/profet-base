﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace LeadsMVC.Models
{
    public class Report
    {
        [Key]
        public long Id { get; set; }
        public string FileName { get; set; }
        public string Path { get; set; }
        public DateTime UploadedDate { get; set; }

        // Relation for users
        [JsonIgnore]
        [ForeignKey("UserId")]
        public virtual ApplicationUser User { get; set; }
        [JsonIgnore]
        public string UserId { get; set; }

    }
}