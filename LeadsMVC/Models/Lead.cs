﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace LeadsMVC.Models
{
    public class Lead
    {
        /**
         * Basic info of the lead
         * Filled when a contact form is filled
         */
        [Key]
        public long Id { get; set; }
        public string Name { get; set; }
        //public string Company { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        //public string Comments { get; set; }
        public string Notes { get; set; }

        /**
         * Extended info of the lead
         * Filled when an agent contacts the lead for more information
         */

        #region Gerenal Data
        // ===========================================================
        public string Genre { get; set; }
        public string CivilStatus { get; set; }
        public string Age { get; set; }
        public string Birthday { get; set; }
        #endregion

        #region Ubication
        public string StreetAndNumber { get; set; }
        public string Colony { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string CP { get; set; }
        #endregion

        #region Labor info
        public string NSE { get; set; }
        public string Position { get; set; }
        public string Company { get; set; }
        public string CompanyType { get; set; }
        public string IndistrySector { get; set; }
        public string Employees { get; set; }
        #endregion

        #region Behavior
        public string Interests { get; set; }
        public string Preferences { get; set; }
        public string Activities { get; set; }
        public string PracticeSports { get; set; }
        public string Habits { get; set; }
        public string Values { get; set; }
        public string Believes { get; set; }
        public string Emotions { get; set; }
        public string BehaviorTags { get; set; }
        #endregion

        #region Buying Authority
        public double BuyingTime { get; set; }
        public double BuyingDecision { get; set; }
        public double NeedDegree { get; set; }
        public double ComparingAgainst { get; set; }
        public string AuthorityTags { get; set; }
        #endregion

        #region Interest
        public DateTime LeadDate { get; set; }
        //public DateTime? LeadHour { get; set; } not needed
        public string InterestedIn { get; set; }
        public string MessageSent { get; set; }
        public string OpportunityValue { get; set; }
        public string ProspectSource { get; set; }
        public string ProsepectGeneratorCompany { get; set; }
        //Actual name: public string Campaign { get; set; }
        public string CampaignName { get; set; }
        public string Comments { get; set; }
        public string InterestTags { get; set; }
        #endregion

        #region Lead Evaluation
        public double Generals { get; set; }
        public double Budget { get; set; }
        public double Job { get; set; }
        public double Authority { get; set; }
        public double Need { get; set; }
        public double TimeFrame { get; set; }
        public double Type { get; set; }
        public double Industry { get; set; }
        public double Status { get; set; }
        public double Contact { get; set; }
        public double QualityScore { get; set; }
        public double Aging { get; set; }
        public double Engagement { get; set; }
        public bool UpsellPotential { get; set; } = false;
        #endregion

        #region Lead score
        public double LeadScore { get; set; }
        #endregion

        #region Convertions
        public string ClosedSell { get; set; }
        [JsonIgnore]
        public double SellApproxAmount { get; set; }
        #endregion

        public bool Called { get; set; } = false;

        /**
         * Relations EF
         * Code First
         */
        [JsonIgnore]
        [ForeignKey("CampaignId")]
        public virtual Campaign Campaign { get; set; }
        [JsonIgnore]
        public long CampaignId { get; set; }

        /**
         * Not mapped properties
         * Custom modeling only
         */
        [NotMapped]
        public bool Pooled { get; set; } = false;
        [NotMapped]
        public bool Filled100
        {
            get
            {
                if (String.IsNullOrEmpty(Name)) return false;
                //if (String.IsNullOrEmpty(Company
                if (String.IsNullOrEmpty(Phone)) return false;
                if (String.IsNullOrEmpty(Email)) return false;
                //if (String.IsNullOrEmpty(Comments
                if (String.IsNullOrEmpty(Notes)) return false;

                /**
                 * Extended info of the lead
                 * Filled when an agent contacts the lead for more information
                 */

                #region Gerenal Data
                // ===========================================================
                if (String.IsNullOrEmpty(Genre)) return false;
                if (String.IsNullOrEmpty(CivilStatus)) return false;
                if (String.IsNullOrEmpty(Age)) return false;
                if (String.IsNullOrEmpty(Birthday)) return false;
                #endregion

                #region Ubication
                if (String.IsNullOrEmpty(StreetAndNumber)) return false;
                if (String.IsNullOrEmpty(Colony)) return false;
                if (String.IsNullOrEmpty(City)) return false;
                if (String.IsNullOrEmpty(State)) return false;
                if (String.IsNullOrEmpty(CP)) return false;
                #endregion

                #region Labor info
                if (String.IsNullOrEmpty(NSE)) return false;
                if (String.IsNullOrEmpty(Position)) return false;
                if (String.IsNullOrEmpty(Company)) return false;
                if (String.IsNullOrEmpty(CompanyType)) return false;
                if (String.IsNullOrEmpty(IndistrySector)) return false;
                //if (Employees == null) return false;
                #endregion

                #region Behavior
                if (String.IsNullOrEmpty(Interests)) return false;
                if (String.IsNullOrEmpty(Preferences)) return false;
                if (String.IsNullOrEmpty(Activities)) return false;
                if (String.IsNullOrEmpty(PracticeSports)) return false;
                if (String.IsNullOrEmpty(Habits)) return false;
                if (String.IsNullOrEmpty(Values)) return false;
                if (String.IsNullOrEmpty(Believes)) return false;
                if (String.IsNullOrEmpty(Emotions)) return false;
                if (String.IsNullOrEmpty(BehaviorTags)) return false;
                #endregion

                #region Buying Authority
                //if (BuyingTime == null) return false;
                //if (BuyingDecision == null) return false;
                //if (NeedDegree == null) return false;
                //if (ComparingAgainst == null) return false;
                if (String.IsNullOrEmpty(AuthorityTags)) return false;
                #endregion

                #region Interest
                if (String.IsNullOrEmpty(InterestedIn)) return false;
                if (String.IsNullOrEmpty(MessageSent)) return false;
                if (String.IsNullOrEmpty(OpportunityValue)) return false;
                if (String.IsNullOrEmpty(ProspectSource)) return false;
                if (String.IsNullOrEmpty(ProsepectGeneratorCompany)) return false;
                //Actual name: if (String.IsNullOrEmpty(Campaign)) return false;
                if (String.IsNullOrEmpty(CampaignName)) return false;
                if (String.IsNullOrEmpty(Comments)) return false;
                #endregion

                return true;
            }
        }

        [NotMapped]
        public bool AttractiveIndustry { get; set; }
        [NotMapped]
        public bool TimeOpportunity
        {
            get { return this.TimeFrame == 100.0; }
        }
        [NotMapped]
        public bool JobAuthority
        {
            get
            {
                double? average =
                    (this.BuyingTime +
                    this.BuyingDecision +
                    this.NeedDegree +
                    this.ComparingAgainst) / 4;

                if (average == null || average < 3.2)
                    return false;
                else
                    return true;
            }

        }
        [NotMapped]
        public bool AllStar
        {
            get
            {
                return (this.UpsellPotential &&
                    this.Filled100 &&
                    this.AttractiveIndustry &&
                    this.TimeOpportunity &&
                    this.JobAuthority);
            }
        }

        /**
         * Validates if the lead has enough contact information
         */
        internal bool IsUsable()
        {
            if (String.IsNullOrEmpty(Email) && String.IsNullOrEmpty(Phone))
                return false;
            else
                return true;
        }
    }
}
