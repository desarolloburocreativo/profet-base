﻿using LeadsMVC.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeadsMVC.Models
{
    public class UserRoles
    {
        public string UserId { get; set; }
        public string UserName { get; set; }
        public string UserFirstName { get; set; }
        public string UserLastName { get; set; }

        // Extended
        public string CompanyName { get; set; }
        public string IndustrySector { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public string Web { get; set; }
        public string Contact { get; set; }
        public string PhoneExt { get; set; }
        public string Mobile { get; set; }

        public string RoleId { get; set; }
        public string RoleName { get; set; }
    }
}
