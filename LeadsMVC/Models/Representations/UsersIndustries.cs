﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeadsMVC.Models
{
    public class UsersIndustries
    {
        public string UserId { get; set; }
        public string UserName { get; set; }
        public string UserFullName { get; set; }

        public long IndustryId { get; set; }
        public string IndustryName { get; set; }
    }
}
