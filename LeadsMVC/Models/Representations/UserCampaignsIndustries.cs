﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeadsMVC.Models
{
    public class UserCampaignsIndustries
    {
        public UserCampaignsIndustries(List<Campaign> campaigns, ApplicationUser user, List<string> industries)
        {
            Campaigns = campaigns;
            User = user;
            Industries = industries;
        }

        public IEnumerable<Campaign> Campaigns { get; set; }
        public IEnumerable<string> Industries { get; set; }
        public ApplicationUser User { get; set; }
    }
}
