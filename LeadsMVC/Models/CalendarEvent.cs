﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeadsMVC.Models
{
    public class CalendarEvent
    {
        [Key]
        public long Id { get; set; }
        public string Title { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public string Color { get; set; }
        public string Description { get; set; }

        /**
         * Relations EF
         * Code First
         */
        [JsonIgnore]
        [ForeignKey("UserId")]
        public virtual ApplicationUser User { get; set; }

        [JsonIgnore]
        public string UserId { get; set; }

        internal bool IsUsable()
        {
            if (String.IsNullOrWhiteSpace(this.Title))
                return false;
            if (this.Start == null || this.Start == DateTime.MinValue)
                return false;
            if (this.End == null || this.End == DateTime.MinValue)
                return false;

            return true;
        }

        internal void convertDatesToUTC6()
        {
            Start = Start.AddHours(6);
            End = End.AddHours(6);
        }
    }
}
