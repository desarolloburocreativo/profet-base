﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace LeadsMVC.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string IndustrySector { get; set; }

        // Extended
        public string CompanyName { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public string Web { get; set; }
        public string Contact { get; set; }
        public string PhoneExt { get; set; }
        public string Mobile { get; set; }

        // Relation one-to-many
        public virtual ICollection<Campaign> Campaigns { get; set; }

        // Relation one-to-many
        public virtual ICollection<Report> Reports { get; set; }

        // Relation many-to-many
        public virtual ICollection<Industry> Industries { get; set; }

        // Relation one-to-many
        [JsonIgnore]
        public virtual ICollection<CalendarEvent> CalendarEvents { get; set; }

        public ApplicationUser() : base()
        {
            Campaigns = new List<Campaign>();
            CalendarEvents = new List<CalendarEvent>();
        }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        // Tables
        public DbSet<Lead> Leads { get; set; }
        public DbSet<Campaign> Campaigns { get; set; }
        public DbSet<Report> Reports { get; set; }
        public DbSet<Industry> Industries { get; set; }
        public DbSet<CalendarEvent> CalendarEvents { get; set; }

        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder); // This needs to go before the other rules!

            // Table renames
            modelBuilder.Entity<ApplicationUser>().ToTable("Users");
            modelBuilder.Entity<IdentityRole>().ToTable("Roles");
            modelBuilder.Entity<IdentityUserRole>().ToTable("UserRoles");
            modelBuilder.Entity<IdentityUserClaim>().ToTable("UserClaims");
            modelBuilder.Entity<IdentityUserLogin>().ToTable("UserLogins");

            // one-to-many
            // Campaign - Leads
            modelBuilder.Entity<Lead>()
                        .HasRequired<Campaign>(s => s.Campaign) // Lead entity requires Campaign
                        //.HasOptional<Campaign>(s => s.Campaign) // Lead entity requires Campaign 
                        .WithMany(s => s.Leads); // Campaign entity includes many Leads entities

            // one-to-many
            // Users - Campaigns
            modelBuilder.Entity<Campaign>()
                        //.HasRequired<ApplicationUser>(s => s.User) // Campaign entity requires ApplicationUser
                        .HasOptional<ApplicationUser>(s => s.User) // Campaign entity requires ApplicationUser 
                        .WithMany(s => s.Campaigns); // ApplicationUser entity includes many Campaign entities

            // one-to-many
            // Users - Reports
            modelBuilder.Entity<Report>()
                        //.HasRequired<ApplicationUser>(s => s.User)
                        .HasOptional<ApplicationUser>(s => s.User)
                        .WithMany(s => s.Reports);

            // one-to-many
            // Users - Calendar events
            modelBuilder.Entity<CalendarEvent>()
                        //.HasRequired<ApplicationUser>(s => s.User)
                        .HasOptional<ApplicationUser>(s => s.User)
                        .WithMany(s => s.CalendarEvents);

            // many-to-many
            // Users - Industries
            modelBuilder.Entity<ApplicationUser>()
                .HasMany<Industry>(s => s.Industries)
                .WithMany(c => c.Users)
                .Map(cs =>
                {
                    cs.MapLeftKey("UserId");
                    cs.MapRightKey("IndustryId");
                    cs.ToTable("UserIndustry");
                });
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }
    }
}