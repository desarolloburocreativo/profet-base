﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeadsMVC.Models
{
    public class Industry
    {
        [Key]
        public long Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        [JsonIgnore]
        [ForeignKey("UserId")]
        public virtual ICollection<ApplicationUser> Users { get; set; }

        public Industry()
        {
            this.Users = new HashSet<ApplicationUser>();
        }
    }
}
