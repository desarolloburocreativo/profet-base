﻿var dataGenerals = {
    labels: [
      "",
      ""
    ],
    datasets: [
      {
          data: [50, 50],
          backgroundColor: [
            "#e8e9ea",
            "#16b09a"
          ],
          hoverBackgroundColor: [
            "#B5B6B7",
            "#007D67"
          ]
      }]
};

var dataContact = {
    labels: [
      "",
      ""
    ],
    datasets: [
      {
          data: [50, 50],
          backgroundColor: [
            "#e8e9ea",
            "#16b09a"
          ],
          hoverBackgroundColor: [
            "#B5B6B7",
            "#007D67"
          ]
      }]
};

var dataJob = {
    labels: [
      "",
      ""
    ],
    datasets: [
      {
          data: [50, 50],
          backgroundColor: [
            "#e8e9ea",
            "#16b09a"
          ],
          hoverBackgroundColor: [
            "#B5B6B7",
            "#007D67"
          ]
      }]
};

var dataType = {
    labels: [
      "",
      ""
    ],
    datasets: [
      {
          data: [50, 50],
          backgroundColor: [
            "#e8e9ea",
            "#16b09a"
          ],
          hoverBackgroundColor: [
            "#B5B6B7",
            "#007D67"
          ]
      }]
};

var dataStatus = {
    labels: [
      "",
      ""
    ],
    datasets: [
      {
          data: [50, 50],
          backgroundColor: [
            "#e8e9ea",
            "#16b09a"
          ],
          hoverBackgroundColor: [
            "#B5B6B7",
            "#007D67"
          ]
      }]
};

var dataQualityScore = {
    labels: [
      "",
      ""
    ],
    datasets: [
      {
          data: [50, 50],
          backgroundColor: [
            "#e8e9ea",
            "#16b09a"
          ],
          hoverBackgroundColor: [
            "#B5B6B7",
            "#007D67"
          ]
      }]
};

var dataAging = {
    labels: [
      "",
      ""
    ],
    datasets: [
      {
          data: [50, 50],
          backgroundColor: [
            "#e8e9ea",
            "#16b09a"
          ],
          hoverBackgroundColor: [
            "#B5B6B7",
            "#007D67"
          ]
      }]
};

var dataEnagement = {
    labels: [
      "",
      ""
    ],
    datasets: [
      {
          data: [50, 50],
          backgroundColor: [
            "#e8e9ea",
            "#16b09a"
          ],
          hoverBackgroundColor: [
            "#B5B6B7",
            "#007D67"
          ]
      }]
};

var globalData = {
    labels: [
      "",
      ""
    ],
    datasets: [
      {
          data: [0, 100],
          backgroundColor: [
            "#f6f6f6",
            "#b0dad1"
          ],
          hoverBackgroundColor: [
            "#f6f6f6",
            "#b0dad1"
          ],
          borderWidth: [
              0,
              0
          ]
      }]
};

var generalsChart = new Chart(document.getElementById('chart-generales'), {
    type: 'doughnut',
    data: dataGenerals,
    options: {
        globalChart: false,
        showPercent: true,
        responsive: true,
        cutoutPercentage: 65,
        legend: {
            display: false
        }
    }
});

var contactChart = new Chart(document.getElementById('chart-contact'), {
    type: 'doughnut',
    data: dataContact,
    options: {
        responsive: true,
        showPercent: true,
        cutoutPercentage: 65,
        legend: {
            display: false
        }
    },
    tooltipCaretSize: 0,
    tooltipTemplate: "<%= value %>"
});

var jobChart = new Chart(document.getElementById('chart-job'), {
    type: 'doughnut',
    data: dataJob,
    options: {
        responsive: true,
        cutoutPercentage: 65,
        legend: {
            display: false
        }
    }
});

var typeChart = new Chart(document.getElementById('chart-type'), {
    type: 'doughnut',
    data: dataType,
    options: {
        responsive: true,
        cutoutPercentage: 65,
        legend: {
            display: false
        }
    }
});

var statusChart = new Chart(document.getElementById('chart-status'), {
    type: 'doughnut',
    data: dataStatus,
    options: {
        responsive: true,
        cutoutPercentage: 65,
        legend: {
            display: false
        }
    }
});

var qualityScoreChart = new Chart(document.getElementById('chart-quality-score'), {
    type: 'doughnut',
    data: dataQualityScore,
    options: {
        responsive: true,
        cutoutPercentage: 65,
        legend: {
            display: false
        }
    }
});

var agingChart = new Chart(document.getElementById('chart-aging'), {
    type: 'doughnut',
    data: dataAging,
    options: {
        responsive: true,
        cutoutPercentage: 65,
        legend: {
            display: false
        }
    }
});

var engagementChart = new Chart(document.getElementById('chart-engagement'), {
    type: 'doughnut',
    data: dataEnagement,
    options: {
        responsive: true,
        cutoutPercentage: 65,
        legend: {
            display: false
        }
    }
});

var globalChart = new Chart(document.getElementById('chart-global'), {
    type: 'doughnut',
    data: globalData,
    options: {
        globalChart: true,
        responsive: true,
        cutoutPercentage: 70,
        legend: {
            display: false
        },
        tooltips: {
            callbacks: {
                label: function (tooltip, data) {
                    return data.datasets[0].data[tooltip.index].toFixed(2) + " %";
                }
            }
        }
    },
    tooltipTemplate: function (d) {
        return d.label + '-' + d.value;
    }
});

Chart.pluginService.register({
    beforeDraw: function (chart) {
        var width = chart.chart.width,
            height = chart.chart.height,
            ctx = chart.chart.ctx;

        if (!chart.options.globalChart) {
            ctx.restore();
            var fontSize = (height / 75).toFixed(2);
            ctx.font = fontSize + "em LatoBold";
            ctx.textBaseline = "middle";
            ctx.fillStyle = "#16b09a";

            var text = chart.options.showPercent ? chart.data.datasets[0].data[1] + "%" : chart.data.datasets[0].data[1]
            var textX = Math.round((width - ctx.measureText(text).width) / 2),
                textY = height / 2;

            ctx.fillText(text, textX, textY);
            ctx.save();
        }

        else {
            ctx.restore();
            var fontSize = (height / 65).toFixed(2);
            ctx.font = fontSize + "em LatoBold";
            ctx.textBaseline = "middle";
            ctx.fillStyle = "#fff";

            var text = chart.data.datasets[0].data[0].toFixed(0) + "%",
                textX = Math.round((width - ctx.measureText(text).width) / 2),
                textY = height / 2;

            ctx.fillText(text, textX, textY);
            ctx.save();
        }
    }
});