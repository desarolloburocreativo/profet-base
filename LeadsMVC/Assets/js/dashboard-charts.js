﻿var data = {
    labels: ["January", "February", "March", "April", "May", "June", "July"],
    datasets: [
        {
            label: "Leads",
            backgroundColor: [
                '#377B90',
                '#377B90',
                '#377B90',
                '#377B90',
                '#377B90',
                '#377B90',
                '#377B90',
                '#377B90',
                '#377B90',
                '#377B90',
                '#377B90',
                '#377B90'
            ],
            borderColor: [
                '#377B90',
                '#377B90',
                '#377B90',
                '#377B90',
                '#377B90',
                '#377B90',
                '#377B90',
                '#377B90',
                '#377B90',
                '#377B90',
                '#377B90',
                '#377B90'
            ],
            borderWidth: 1,
            data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0 , 0, 0],
        }
    ]
};

var leadsChart = new Chart(document.getElementById('chart-dashboard-leads'), {
    type: 'bar',
    data: data,
    options: {
        defaultFontSize: 1,
        scaleShowLabels : false,
        responsive: true,
        legend: {
            display: false
        },
        scales: {
            xAxes: [{
                display: false,
                gridLines: {
                    display: false
                },
                ticks: {
                    display: false
                },
                barPercentage: 0.9,
                categoryPercentage: 1.0
            }],
            yAxes: [{
                display: false,
                gridLines: {
                    display: false,
                },
                ticks: {
                    suggestedMax: 30
                }
            }]
        }
    }
});