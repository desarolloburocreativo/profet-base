﻿function getBText(value) {
    if (value <= 25)
        return "No defined budget"
    else if (value <= 50)
        return "Would have to make a big effort"
    else if (value <= 75)
        return "Needs a slight budget adjustment"
    else if (value <= 100)
        return "Has the budget"
}

function getAText(value) {
    if (value <= 25)
        return "No influence over decision maker"
    else if (value <= 50)
        return "Little influence over decision maker"
    else if (value <= 75)
        return "Has influence over decision maker"
    else if (value <= 100)
        return "Is the decision maker"
}

function getNText(value) {
    if (value <= 25)
        return "Problem identified"
    else if (value <= 50)
        return "Problem identified, exploring solutions"
    else if (value <= 75)
        return "Solution identified, not sure it’s right for them"
    else if (value <= 100)
        return "Looking for quote/offer"
}

function getTText(value) {
    if (value <= 25)
        return "No buying time frame"
    else if (value <= 50)
        return "Purchase in 6+ months"
    else if (value <= 75)
        return "Purchase in 3-6 months"
    else if (value <= 100)
        return "Purchase in 1-3 months"
}

function getContactText(value) {
    if (value <= 25)
        return "No contact"
    else if (value <= 50)
        return "Initial contact - call rescheduled"
    else if (value <= 75)
        return "First contact - Info given"
    else if (value <= 100)
        return "First contact - Ready to buy"
}

function getTypeText(value) {
    if (value <= 25)
        return "Start up"
    else if (value <= 50)
        return "Small size company"
    else if (value <= 75)
        return "Medium size company"
    else if (value <= 100)
        return "Large size company"
}

function getStatusText(value) {
    if (value <= 25)
        return "Just exploring"
    else if (value <= 50)
        return "Needs to buy, but is not in a rush"
    else if (value <= 75)
        return "Needs to buy soon"
    else if (value <= 100)
        return "In a big rush to buy"
}

function getJobText(value) {
    if (value <= 25)
        return "Mid Level Employee"
    else if (value <= 50)
        return "Mid Level Employee"
    else if (value <= 75)
        return "Manager/Director"
    else if (value <= 100)
        return "C-level/Owner"
}

function getQualityText(value) {
    if (value <= 25)
        return "Low quality information provided"
    else if (value <= 50)
        return "Medium quality information provided"
    else if (value <= 75)
        return "High quality information provided"
    else if (value <= 100)
        return "Excellent quality information provided"
}

function getAgingText(value) {
    if (value <= 25)
        return "Over 6 months"
    else if (value <= 50)
        return "3-6 months"
    else if (value <= 75)
        return "1-3 months"
    else if (value <= 100)
        return "<1 month"
}

function getEngagementText(value) {
    if (value <= 25)
        return "Not interested"
    else if (value <= 50)
        return "Little interest"
    else if (value <= 75)
        return "Interested"
    else if (value <= 100)
        return "Very interested"
}

