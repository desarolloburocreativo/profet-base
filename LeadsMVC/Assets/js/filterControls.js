﻿var date1 = "", date2 = "";
var monthNames = [
        "Jan", "Feb", "Mar",
        "Apr", "May", "Jun", "Jul",
        "Aug", "Sep", "Oct",
        "Nov", "Dec"
];

Date.prototype.toHHMM = function () {
    var hour = this.getHours();
    var minutes = this.getMinutes();
    return pad(hour, 2) + ":" + pad(minutes, 2);
}

function pad(n, width, z) {
    z = z || '0';
    n = n + '';
    return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
}

function initFilterControls() {
    // Init campaigns
    initCampaigns();

    // Init datepicker
    initDatePicker();

    // Init Leads segmentation
    initLeadsSegmentation();

    // Init filters options
    initFilters();
}

function initFilters() {
    // Setup segmented selection for datepicker
    $('#segmented-date li').on('click', function () {
        $('#segmented-date li').each(function (i, e) {
            $(e).removeClass('active');
        });

        $(this).addClass('active');
        return false;
    });
}

function initLeadsSegmentation() {
    // Setup segmented selection for leads
    $('#segmented-leads li').on('click', function () {
        if ($(this).hasClass("active"))
            return false;

        $('#segmented-leads li').each(function (i, e) {
            $(e).removeClass('active');
        });

        $(this).addClass('active');

        $.event.trigger({
            type: "segmentationChanged",
            segmentation: $('#segmented-leads li.active').data("segmentation")
        });

        return false;
    });
}

function getSelectedSegmentation() {
    return $('#segmented-leads li.active').data("segmentation");
}

function initCampaigns() {
    var MY_LEADS = 47765;
    $.get("../api/campaigns?code=" + MY_LEADS, function (campaignsArray) {
        fillCampaignsList(campaignsArray);
    });
}

function fillCampaignsList(campaigns) {
    $("#campaigns-select").empty();

    // Add select all
    $("#campaigns-select").append($('<option>', {
        value: -1,
        text: "Todas"
    }));

    $.each(campaigns, function (i, campaign) {
        $('#campaigns-select').append($('<option>', {
            value: campaign.id,
            text: campaign.name
        }));
    });

    $.event.trigger({
        type: "campaignsLoaded",
        campaigns: campaigns
    });
}

function initDatePicker() {
    // Date format configs
    $.datepicker.regional['es'] = {
        dateFormat: 'dd/mm/yy',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''
    };
    $.datepicker.setDefaults($.datepicker.regional['es']);

    // Actual initiation of datepicker
    jQuery('.datepicker').datepicker({
        numberOfMonths: 1,
        showButtonPanel: true,
        beforeShowDay: function (date) {
            var date1 = $.datepicker.parseDate($.datepicker._defaults.dateFormat, $("#inputDate1").val());
            var date2 = $.datepicker.parseDate($.datepicker._defaults.dateFormat, $("#inputDate2").val());
            return [true, date1 && ((date.getTime() == date1.getTime()) || (date2 && date >= date1 && date <= date2)) ? "dp-highlight" : ""];
        },
        onSelect: function (dateText, inst) {
            var date1 = $.datepicker.parseDate($.datepicker._defaults.dateFormat, $("#inputDate1").val());
            var date2 = $.datepicker.parseDate($.datepicker._defaults.dateFormat, $("#inputDate2").val());
            var selectedDate = $.datepicker.parseDate($.datepicker._defaults.dateFormat, dateText);

            if (!date1 || date2) {
                $("#inputDate1").val(dateText);
                $("#inputDate2").val("");
                $(this).datepicker();
            } else if (selectedDate < date1) {
                $("#inputDate2").val($("#inputDate1").val());
                $("#inputDate1").val(dateText);
                $(this).datepicker();
                $('.datepicker').addClass('hidden');

                setDateRangeText(selectedDate, date1);
            } else {
                $("#inputDate2").val(dateText);
                $(this).datepicker();
                $('.datepicker').addClass('hidden');

                setDateRangeText(date1, selectedDate);
            }
        }
    });

    // Showing up on click
    $('#date-range').click(function () {
        $('.datepicker').removeClass('hidden');
    });

    // Showing up on click
    $('#30-days').click(function () {
        var date1 = new Date();
        var date2 = new Date();

        date2.setDate(date1.getDate() - 30);
        setDateRangeText(date2, date1);
    });

    // Showing up on click
    $('#7-days').click(function () {
        var date1 = new Date();
        var date2 = new Date();

        date2.setDate(date1.getDate() - 7);
        setDateRangeText(date2, date1);
    });

    // When click outside
    $(document).mouseup(function (e) {
        var container = $(".datepicker");

        if (!container.is(e.target) // if the target of the click isn't the container...
            && container.has(e.target).length === 0) // ... nor a descendant of the container
        {
            $('.datepicker').addClass('hidden');
        }
    });
}

function setDateRangeText(date1, date2) {
    var fromDateString = monthNames[date1.getMonth()] + " " + date1.getDate() + ", " + date1.getFullYear();
    var toDateString = monthNames[date2.getMonth()] + " " + date2.getDate() + ", " + date2.getFullYear();

    $("#date-desc").html("<label><b>" + fromDateString + " - " + toDateString + "</b></label> <span class='caret'></span>");

    setDates(date1, date2);
    $.event.trigger({
        type: "datesChanged",
        date1: date1,
        date2: date2
    });
}

function getSelectedCampaignId() {
    return $("#campaigns-select option:selected").val();
}

function getDates() {
    return [date1, date2];
}

function setDates(from, to) {
    date1 = from;
    date2 = to;
}