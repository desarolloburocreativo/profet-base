﻿using LeadsMVC.Models;
using LeadsMVC.Utils;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LeadsMVC.Controllers
{
    public class CalendarController : Controller
    {
        // GET: Calendar
        public ActionResult Index()
        {
            return View();
        }

        [Authorize(Roles = "Admin, Dev")]
        public ActionResult Admin()
        {
            return View();
        }

        [HttpPost]
        [Authorize(Roles = "Admin, Dev")]
        public ActionResult MakeEvent([System.Web.Http.FromBody]CalendarEvent calendarEvent)
        {
            if (calendarEvent.IsUsable())
            {
                using (var context = new ApplicationDbContext())
                {
                    context.CalendarEvents.Add(calendarEvent);
                    context.SaveChanges();

                    return Json(new { success = true, responseText = "Event saved" }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                Response.StatusCode = 400;
                return Json(new { success = false, responseText = "Event is not valid" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPut]
        [Authorize(Roles = "Admin, Dev")]
        public void EditEvent(int id, [System.Web.Http.FromBody]CalendarEvent calendarEvent)
        {
            if (calendarEvent.IsUsable())
            {
                using (var context = new ApplicationDbContext())
                {
                    context.CalendarEvents.Attach(calendarEvent);
                    context.Entry(calendarEvent).Property(x => x.Title).IsModified = true;
                    context.Entry(calendarEvent).Property(x => x.Description).IsModified = true;
                    context.Entry(calendarEvent).Property(x => x.Start).IsModified = true;
                    context.Entry(calendarEvent).Property(x => x.End).IsModified = true;
                    context.SaveChanges();
                }
            }
            else
            {
                Response.StatusCode = 400;
                return;
            }
        }

        [HttpGet]
        [Authorize(Roles = "Admin, Dev")]
        public JsonCamelCaseResult GetEvents()
        {
            using (var context = new ApplicationDbContext())
            {
                var events = from user in context.Users
                             join ev in context.CalendarEvents on user.Id equals ev.UserId
                             select new { ev.Id, ev.Title, ev.Description, ev.Start, ev.End, user.UserName, UserId = user.Id, UserFullName = user.FirstName + " " + user.LastName };

                return new JsonCamelCaseResult(events.ToList(), JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        [Authorize]
        public JsonCamelCaseResult GetMyEvents()
        {
            using (var context = new ApplicationDbContext())
            {
                var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
                var user = userManager.FindById(User.Identity.GetUserId());

                var events = from u in context.Users
                             join ev in context.CalendarEvents on user.Id equals ev.UserId
                             where u.Id == user.Id
                             select new { ev.Id, ev.Title, ev.Description, ev.Start, ev.End, user.UserName, UserId = user.Id, UserFullName = user.FirstName + " " + user.LastName, ev.Color };

                return new JsonCamelCaseResult(events.ToList(), JsonRequestBehavior.AllowGet);
            }
        }

        [HttpDelete]
        [Authorize(Roles = "Admin, Dev")]
        public void DeleteEvent(long id)
        {
            using (var context = new ApplicationDbContext())
            {
                var calendarEvent = context.CalendarEvents.SingleOrDefault(ce => ce.Id == id); //returns a single item.
                if (calendarEvent != null)
                {
                    context.CalendarEvents.Remove(calendarEvent);
                    context.SaveChanges();
                    return;
                }
                else
                {
                    Response.StatusCode = 400;
                    return;
                }
            }
        }
    }
}