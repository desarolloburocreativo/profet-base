﻿using LeadsMVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace LeadsMVC.Controllers
{
    public class UsersController : ApiController
    {
        // GET api/<controller>
        [Authorize(Roles = "Admin, Dev")]
        public IEnumerable<Object> Get()
        {
            using (var context = new ApplicationDbContext())
            {
                var query = from user in context.Users
                            select new { user.Id, user.UserName, UserFullName = user.FirstName + " " + user.LastName };
                return query.ToList();
            }
        }

        // GET api/<controller>/5
        [Authorize(Roles = "Admin, Dev")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<controller>
        public void Post([FromBody]string value)
        {
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}