﻿using LeadsMVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LeadsMVC.Controllers
{
    [Authorize(Roles = "Admin, Dev, Capturer")]
    public class CapturerController : Controller
    {
        // GET: Capturer
        public ActionResult Index()
        {
            using (var context = new ApplicationDbContext())
            {
                var lead = from b in context.Leads
                           //where b.Id.Equals(id)
                           select b;

                return View();
            }
        }

        [HttpPost]
        [Authorize(Roles = "Admin, Dev")]
        public ActionResult FillLead([System.Web.Http.FromBody]Lead lead)
        {
            if (lead.IsUsable())
            {
                using (var context = new ApplicationDbContext())
                {
                    context.Leads.Attach(lead);
                    //context.Entry(lead).Property(x => x.Name).IsModified = true;
                    //context.Entry(lead).Property(x => x.Description).IsModified = true;
                    context.SaveChanges();

                    return Json(new { success = true, responseText = "Event saved" }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                Response.StatusCode = 400;
                return Json(new { success = false, responseText = "Event is not valid" }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}