﻿using LeadsMVC.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.Routing;
using System.Web.Routing;

namespace LeadsMVC.Controllers
{
    public class LeadsApiController : ApiController
    {
        // Leads
        // =====================================================================================

        // GET api/<controller>
        [Authorize(Roles = "Admin, Dev")]
        public IEnumerable<Lead> Get()
        {
            using (var context = new ApplicationDbContext())
            {
                //var leads = context.Leads.SqlQuery("SELECT * FROM dbo.Leads").ToList();
                var leads = context.Leads.ToList();
                return leads;
            }
        }

        // GET api/<controller>/5
        [Authorize]
        public Lead Get(int id)
       {
            using (var context = new ApplicationDbContext())
            {
                var lead = from b in context.Leads
                           where b.Id.Equals(id)
                           select b;

                return lead.FirstOrDefault();
            }
        }

        // GET api/<controller>/tons-of-params
        [Authorize(Roles = "Admin, Dev")]
        public Object Get(string code, long campaign, int page = 1, int pageSize = 10, string search = null, string date1 = null, string date2 = null, string sortScore = null, string sortDate = null)
        {
            using (var context = new ApplicationDbContext())
            {
                // To not confuse myself
                page = page - 1;

                // fix
                if (sortScore == "undefined")
                    sortScore = null;
                if (sortDate == "undefined")
                    sortDate = null;

                // Optional values stuff
                // =========================================================================
                DateTime dateTime1, dateTime2;
                if (date1 != null) { dateTime1 = parseJSDate(date1); }
                else { dateTime1 = DateTime.MinValue; }
                if (date2 != null) { dateTime2 = parseJSDate(date2); }
                else { dateTime2 = DateTime.MaxValue; }

                // =========================================================================

                // Set campaigns we are going to use
                List<long?> campaigns = new List<long?>();
                if (campaign == -1) { campaigns = getAllCampaigns(context); }
                else { campaigns.Add(campaign); }

                return leadsFromAllCampaigns(campaigns, page, pageSize, search, dateTime1, dateTime2, sortScore, sortDate, context);
            }
        }

        // GET api/<controller>/tons-of-params
        [Authorize]
        public Object Get(long campaign, int page = 1, int pageSize = 10, string search = null, string date1 = null, string date2 = null, string segmentation = null, string sortScore = null, string sortDate = null)
        {
            using (var context = new ApplicationDbContext())
            {
                // To not confuse myself
                page = page - 1;

                // fix
                if (sortScore == "undefined")
                    sortScore = null;
                if (sortDate == "undefined")
                    sortDate = null;

                // Optional values stuff
                // =========================================================================
                DateTime dateTime1, dateTime2;
                if (date1 != null) { dateTime1 = parseJSDate(date1); }
                else { dateTime1 = DateTime.MinValue; }
                if (date2 != null) { dateTime2 = parseJSDate(date2); }
                else { dateTime2 = DateTime.MaxValue; }

                // =========================================================================

                // Set campaigns we are going to use
                List<long?> campaigns = new List<long?>();
                if (campaign == -1) { campaigns = getAllUserCampaignsIds(context); }
                else { campaigns.Add(campaign); }

                return leadsFromAllCampaigns(campaigns, page, pageSize, search, dateTime1, dateTime2, segmentation, sortScore, sortDate, context);
            }
        }

        // POST api/<controller>
        [ResponseType(typeof(long))]
        [HttpPost]
        public IHttpActionResult Post([FromBody]Lead lead)
        {
            using (var context = new ApplicationDbContext())
            {
                if (lead.IsUsable() && CampaignExists(lead.CampaignId, context))
                {
                    // Set lead date
                    lead.LeadDate = DateTime.Now;
                    lead.CampaignName = CampaignNameForId(lead.CampaignId);

                    context.Leads.Add(lead);
                    context.SaveChanges();

                    HttpContext.Current.Response.AddHeader("Lead-ID", lead.Id.ToString());
                    return Content(HttpStatusCode.OK, lead);
                    //HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
                    //return response;
                }
                else
                {
                    return Content(HttpStatusCode.BadRequest, lead);
                    //HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.BadRequest);
                    //return response;
                }
            }

        }

        // PUT api/<controller>/5
        [Authorize]
        [HttpPut]
        public void Put(int id, [FromBody]Lead lead)
        {
            try
            {
                using (var context = new ApplicationDbContext())
                {
                    if (id == Utils.Constants.Codes.PutNotes)
                    {
                        if (leadBelongsToUser(lead, context))
                        {
                            var result = context.Leads.SingleOrDefault(l => l.Id == lead.Id);
                            if (result != null)
                            {
                                result.Notes = lead.Notes;
                                context.SaveChanges();
                            }
                        }
                    }
                    if (id == 666)
                    {
                        var result = context.Leads.SingleOrDefault(l => l.Id == lead.Id);
                        if (result != null)
                        {
                            result.Called = lead.Called;
                            context.SaveChanges();
                        }
                    }

                    if (id == 420)
                    {
                        
                    }
                }
            }
            catch (Exception ex) { }
        }

        // DELETE api/<controller>/5
        [Authorize]
        public void Delete(int id)
        {
        }


        #region private methods
        private bool CampaignExists(long campaignId, ApplicationDbContext context)
        {
            if (context.Campaigns.Any(c => c.Id == campaignId))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private string CampaignNameForId(long campaignId)
        {
            using (var context = new ApplicationDbContext())
            {
                return context.Campaigns.Where(c => c.Id == campaignId).Select(c => c.Name).FirstOrDefault();
            }
        }

        private Object leadsFromAllCampaigns(List<long?> campaigns, int page, int pageSize, string search, DateTime date1, DateTime date2, string segmentation, string sortScore, string sortDate, ApplicationDbContext context)
        {
            // Own results
            var results = context.Leads.Where(l => l.Name != null && campaigns.Contains(l.CampaignId));

            // Segmentation
            if (segmentation == Utils.Constants.Segmentation.Pool || segmentation == Utils.Constants.Segmentation.All)
            {
                // Current user
                var user = UserManager(context).FindById(User.Identity.GetUserId());

                // Current user industries ids
                var currentIndustries = context.Industries
                                    .Where(i => i.Users.Any(u => u.Id == user.Id))
                                    .Select(i => i.Id)
                                    .ToList();

                // Get all users in role "User" who have common industries with current
                var roleUserId = context.Roles.Where(r => r.Name == "User").Select(r => r.Id).FirstOrDefault();
                var users = context.Users
                                    .Where(u => u.Roles.Any(r => r.RoleId == roleUserId)) // Only normal users
                                    .Where(u => u.Industries.Any(i => currentIndustries.Contains(i.Id))) // Who have current user idustries
                                    .Where(u => u.Id != user.Id) // Exclude current user
                                    .Select(u => u.Id)
                                    .ToList();

                // Get all users campaigns
                var usersCampaigns = context.Campaigns
                                    .Where(c => users.Contains(c.UserId))
                                    .Select(c => c.Id)
                                    //.Cast<long?>()
                                    .ToList();

                // Get all leads in campaigns
                var poolLeads = context.Leads
                                    .Where(l => usersCampaigns.Contains(l.CampaignId)) // Get leads who are on users campaigns
                                    .Where(l => l.Name != null); // Just to make sure it doesnt bring null shit

                // Set pooled leads
                poolLeads.ToList().ForEach(l => l.Pooled = true);

                if (segmentation == Utils.Constants.Segmentation.Pool)
                {
                    results = poolLeads;
                }

                else if (segmentation == Utils.Constants.Segmentation.All)
                {
                    var union = results.Union(poolLeads);
                    results = union;
                }
            }

            // Filters
            // =========================================================================

            // Search
            if (!String.IsNullOrWhiteSpace(search))
            {
                // Trim before search
                search = search.Trim();
                results = results.Where(l => (l.Name.Contains(search)
                        || l.Email.Contains(search)
                        || l.City.Contains(search))
                        || l.Phone.Contains(search));
            }

            // Dates
            if (!(date1 == DateTime.MinValue && date2 == DateTime.MaxValue))
                results = results.Where(l => l.LeadDate >= date1 && l.LeadDate <= date2);

            // =========================================================================

            // Mod lead score
            //results.ToList().ForEach(l =>
            //{
            //    // Average
            //    l.LeadScore =
            //        (l.Generals +
            //        l.Budget +
            //        l.Authority +
            //        l.Need +
            //        l.TimeFrame +
            //        l.Job +
            //        l.Type +
            //        l.Status +
            //        l.Contact +
            //        l.QualityScore +
            //        l.Aging +
            //        l.Engagement) / 12;
            //});

            //context.SaveChanges();

            int totalRecords = results.Count();
            var totalPages = (int)Math.Ceiling((double)totalRecords / pageSize);

            // Sort
            if (sortScore != null)
            {
                if (sortScore == Utils.Constants.Sort.DESC)
                    results = results.OrderByDescending(l => l.LeadScore).Skip(page * pageSize).Take(pageSize);
                else if (sortScore == Utils.Constants.Sort.ASC)
                    results = results.OrderBy(l => l.LeadScore).Skip(page * pageSize).Take(pageSize);
            }
            else if (sortDate != null)
            {
                if (sortDate == Utils.Constants.Sort.DESC)
                    results = results.OrderByDescending(l => l.LeadDate).Skip(page * pageSize).Take(pageSize);
                else if (sortDate == Utils.Constants.Sort.ASC)
                    results = results.OrderBy(l => l.LeadDate).Skip(page * pageSize).Take(pageSize);
            }

            else
            {
                results = results.OrderBy(l => l.Id).Skip(page * pageSize).Take(pageSize);
            }

            var userIndustries = getAllUserIndustries(context);
            var resultsList = results.ToList();
            resultsList.ForEach(l =>
            {
                l.AttractiveIndustry = leadIndustryIsInUserCampaign(l, userIndustries);
            });

            return new
            {
                Page = page + 1,
                TotalRecords = totalRecords,
                TotalPages = totalPages,
                Results = resultsList
                //Results = results
                //            //.OrderBy(l => l.Id)
                //            //.Skip(page * pageSize)
                //            //.Take(pageSize)
                //            .ToList()
            };
        }

        private Object leadsFromAllCampaigns(List<long?> campaigns, int page, int pageSize, string search, DateTime date1, DateTime date2, string sortScore, string sortDate, ApplicationDbContext context)
        {
            // Own results
            var results = context.Leads.Where(l => l.Name != null && campaigns.Contains(l.CampaignId));

            // Filters
            // =========================================================================

            // Search
            if (!String.IsNullOrWhiteSpace(search))
            {
                // Trim before search
                search = search.Trim();
                results = results.Where(l => (l.Name.Contains(search)
                        || l.Email.Contains(search)
                        || l.City.Contains(search))
                        || l.Phone.Contains(search));
            }

            // Dates
            if (!(date1 == DateTime.MinValue && date2 == DateTime.MaxValue))
                results = results.Where(l => l.LeadDate >= date1 && l.LeadDate <= date2);

            // =========================================================================

            int totalRecords = results.Count();
            var totalPages = (int)Math.Ceiling((double)totalRecords / pageSize);

            // Sort
            if (sortScore != null)
            {
                if (sortScore == Utils.Constants.Sort.DESC)
                    results = results.OrderByDescending(l => l.LeadScore).Skip(page * pageSize).Take(pageSize);
                else if (sortScore == Utils.Constants.Sort.ASC)
                    results = results.OrderBy(l => l.LeadScore).Skip(page * pageSize).Take(pageSize);
            }
            else if (sortDate != null)
            {
                if (sortDate == Utils.Constants.Sort.DESC)
                    results = results.OrderByDescending(l => l.LeadDate).Skip(page * pageSize).Take(pageSize);
                else if (sortDate == Utils.Constants.Sort.ASC)
                    results = results.OrderBy(l => l.LeadDate).Skip(page * pageSize).Take(pageSize);
            }

            else
            {
                results = results.OrderBy(l => l.Id).Skip(page * pageSize).Take(pageSize);
            }

            var userIndustries = getAllUserIndustries(context);
            var resultsList = results.ToList();
            resultsList.ForEach(l =>
            {
                l.AttractiveIndustry = leadIndustryIsInUserCampaign(l, userIndustries);
            });

            return new
            {
                Page = page + 1,
                TotalRecords = totalRecords,
                TotalPages = totalPages,
                Results = resultsList
                //Results = results
                //            //.OrderBy(l => l.Id)
                //            //.Skip(page * pageSize)
                //            //.Take(pageSize)
                //            .ToList()
            };
        }

        private List<long?> getAllCampaigns(ApplicationDbContext context)
        {
            var campaigns = context.Campaigns
                                .Select(c => c.Id)
                                .Cast<long?>()
                                .ToList();
            return campaigns;
        }

        private List<long?> getAllUserCampaignsIds(ApplicationDbContext context)
        {
            var user = UserManager(context).FindById(User.Identity.GetUserId());
            var campaigns = context.Campaigns
                                .Where(c => c.UserId == user.Id)
                                .Select(c => c.Id)
                                .Cast<long?>()
                                .ToList();
            return campaigns;
        }

        private List<UsersIndustries> getAllUserIndustries(ApplicationDbContext context)
        {
            var user = UserManager(context).FindById(User.Identity.GetUserId());
            var userIndustries = context.Database.SqlQuery<UsersIndustries>(
                       @"SELECT u.Id AS UserID, u.UserName, CONCAT(u.FirstName, ' ', u.LastName) AS UserFullName, i.Name AS IndustryName, i.Id AS IndustryId
                        FROM UserIndustry ui
                        JOIN Users u on ui.UserId = u.Id
                        JOIN Industries i on ui.IndustryId = i.Id
                        WHERE u.Id = '" + user.Id + "'").ToList();

            return userIndustries;
        }

        private DateTime parseJSDate(string date)
        {
            return DateTime.ParseExact(date.Substring(0, 24),
                              "ddd MMM d yyyy HH:mm:ss",
                              CultureInfo.InvariantCulture);
        }

        private UserManager<ApplicationUser> UserManager(ApplicationDbContext context)
        {
            return new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
        }

        private bool leadBelongsToUser(Lead lead, ApplicationDbContext context)
        {
            var userCampaigns = getAllUserCampaignsIds(context);
            var userLeads = context.Leads.Where(l => l.Name != null && userCampaigns.Contains(l.CampaignId)).Select(l => l.Id);
            return userLeads.ToList().Contains(lead.Id);
        }

        private bool leadIndustryIsInUserCampaign(Lead lead, ApplicationDbContext context)
        {
            var industries = getAllUserIndustries(context);
            foreach(var industry in industries)
            {
                if (industry.IndustryName.ToLower().Contains(lead.IndistrySector.ToLower()))
                    return true;
            }

            return false;

        }

        private bool leadIndustryIsInUserCampaign(Lead lead, List<UsersIndustries> industries)
        {
            foreach (var industry in industries)
            {
                if (lead.IndistrySector == null)
                    continue;
                if (industry.IndustryName.ToLower().Contains(lead.IndistrySector.ToLower()))
                    return true;
            }

            return false;
        }

        #endregion
    }
}
