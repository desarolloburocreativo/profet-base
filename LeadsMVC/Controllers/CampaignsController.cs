﻿using LeadsMVC.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace LeadsMVC.Controllers
{
    [Authorize]
    public class CampaignsController : ApiController
    {
        // GET api/<controller>
        [Authorize(Roles = "Admin, Dev")]
        public IEnumerable<Object> Get()
        {
            using (var context = new ApplicationDbContext())
            {
                var query = from user in context.Users
                            join campaign in context.Campaigns on user.Id equals campaign.UserId
                            select new { campaign.Id, campaign.Name, campaign.Description,
                                campaign.AccountMgmt, campaign.AdminBudget, campaign.Clicks, campaign.Community, campaign.Engagement, campaign.Impressions, campaign.MediaBudget,
                                campaign.SalesRep, campaign.ServiceCost, campaign.ConfigurationUrl, campaign.StatmentUrl, campaign.ContractUrl,
                                user.UserName, UserId = user.Id, UserFullName = user.FirstName + " " + user.LastName };

                return query.ToList();
            }
        }

        // GET api/<controller?code=
        [Authorize]
        public IEnumerable<Object> Get(int code)
        {
            // Current user campaigns
            if (code == Utils.Constants.Codes.MyCampaigns)
            {
                using (var context = new ApplicationDbContext())
                {
                    var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
                    var user = userManager.FindById(User.Identity.GetUserId());

                    var campaigns = context.Campaigns
                                        .Where(c => c.UserId == user.Id)
                                        .ToList();

                    return campaigns;
                }
            }

            else
            {
                return Enumerable.Empty<Object>();
            }
        }

        // POST api/<controller>
        [Authorize(Roles = "Admin, Dev")]
        public HttpResponseMessage Post([FromBody]Campaign campaign)
        {
            if (!String.IsNullOrWhiteSpace(campaign.Name))
            {
                using (var context = new ApplicationDbContext())
                {
                    context.Campaigns.Add(campaign);
                    context.SaveChanges();

                    return Request.CreateResponse(HttpStatusCode.OK);
                }
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }

        // PUT api/<controller>/5
        [Authorize(Roles = "Admin, Dev")]
        public HttpResponseMessage Put(int id, [FromBody]Campaign campaign)
        {
            if (String.IsNullOrWhiteSpace(campaign.Name))
                return Request.CreateResponse(HttpStatusCode.BadRequest);

            using (var context = new ApplicationDbContext())
            {
                context.Campaigns.Attach(campaign);
                context.Entry(campaign).Property(x => x.Name).IsModified = true;
                context.Entry(campaign).Property(x => x.Description).IsModified = true;

                context.Entry(campaign).Property(x => x.ServiceCost).IsModified = true;
                context.Entry(campaign).Property(x => x.Impressions).IsModified = true;
                context.Entry(campaign).Property(x => x.LeadsCount).IsModified = true;
                context.Entry(campaign).Property(x => x.Clicks).IsModified = true;
                context.Entry(campaign).Property(x => x.Engagement).IsModified = true;
                context.Entry(campaign).Property(x => x.Community).IsModified = true;
                context.Entry(campaign).Property(x => x.AdminBudget).IsModified = true;
                context.Entry(campaign).Property(x => x.MediaBudget).IsModified = true;
                context.Entry(campaign).Property(x => x.SalesRep).IsModified = true;
                context.Entry(campaign).Property(x => x.AccountMgmt).IsModified = true;
                context.Entry(campaign).Property(x => x.ConfigurationUrl).IsModified = true;
                context.Entry(campaign).Property(x => x.ContractUrl).IsModified = true;
                context.Entry(campaign).Property(x => x.StatmentUrl).IsModified = true;
                context.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK);
            }
        }

        // DELETE api/<controller>/5
        [Authorize(Roles = "Admin, Dev")]
        public HttpResponseMessage Delete(long id)
        {
            using (var context = new ApplicationDbContext())
            {
                var campaign = context.Campaigns.SingleOrDefault(c => c.Id == id); //returns a single item.
                if (campaign != null)
                {
                    context.Campaigns.Remove(campaign);
                    context.SaveChanges();
                    return Request.CreateResponse(HttpStatusCode.OK);
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest);
                }
            }
        }
    }
}