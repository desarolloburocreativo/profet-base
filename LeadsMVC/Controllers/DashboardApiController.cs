﻿using LeadsMVC.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace LeadsMVC.Controllers
{
    [Authorize]
    public class DashboardApiController : ApiController
    {
        // Dashboard
        // =====================================================================================
        public Object Get(long campaign, string date1 = null, string date2 = null, string segmentation = null)
        {
            // Optional values stuff
            // =========================================================================
            DateTime dateTime1, dateTime2;
            if (!(String.IsNullOrEmpty(date1))) { dateTime1 = parseJSDate(date1); }
            else { dateTime1 = DateTime.MinValue; }
            if (!(String.IsNullOrEmpty(date1))) { dateTime2 = parseJSDate(date2); }
            else { dateTime2 = DateTime.MaxValue; }

            // =========================================================================

            // Set campaigns we are going to use
            List<long?> campaigns = new List<long?>();
            if (campaign == -1) { campaigns = getAllUserCampaignsIds(); }
            else { campaigns.Add(campaign); }

            return GetCampaignsStatus(campaigns, dateTime1, dateTime2, segmentation);
        }

        // Campaigns is a list in case all the campaigns are selected
        private Object GetCampaignsStatus(List<long?> campaigns, DateTime date1, DateTime date2, string segmentation)
        {
            using (var context = new ApplicationDbContext())
            {
                // Build query
                // =============================================================================================================================

                // Own results
                var campaignsList = context.Campaigns.Where(c => campaigns.Contains(c.Id)).ToList();
                var results = context.Leads.Where(l => l.Name != null && campaigns.Contains(l.CampaignId));

                // Segmentation
                if (segmentation == Utils.Constants.Segmentation.Pool || segmentation == Utils.Constants.Segmentation.All)
                {
                    // Current user
                    var user = UserManager(context).FindById(User.Identity.GetUserId());

                    // Current user industries ids
                    var currentIndustries = context.Industries
                                        .Where(i => i.Users.Any(u => u.Id == user.Id))
                                        .Select(i => i.Id)
                                        .ToList();

                    // Get all users in role "User" who have common industries with current
                    var roleUserId = context.Roles.Where(r => r.Name == "User").Select(r => r.Id).FirstOrDefault();
                    var users = context.Users
                                        .Where(u => u.Roles.Any(r => r.RoleId == roleUserId)) // Only normal users
                                        .Where(u => u.Industries.Any(i => currentIndustries.Contains(i.Id))) // Who have current user idustries
                                        .Where(u => u.Id != user.Id) // Exclude current user
                                        .Select(u => u.Id)
                                        .ToList();

                    // Get all users campaigns
                    var usersCampaigns = context.Campaigns
                                        .Where(c => users.Contains(c.UserId))
                                        .Select(c => c.Id)
                                        //.Cast<long?>()
                                        .ToList();

                    // Get all leads in campaigns
                    var poolLeads = context.Leads
                                        .Where(l => usersCampaigns.Contains(l.CampaignId)) // Get leads who are on users campaigns
                                        .Where(l => l.Name != null); // Just to make sure it doesnt bring null shit

                    var poolLeadsCount = poolLeads.Count();
                    var resultsCount = results.Count();

                    // Set pooled leads
                    poolLeads.ToList().ForEach(l => l.Pooled = true);

                    if (segmentation == Utils.Constants.Segmentation.Pool)
                    {
                        results = poolLeads;
                    }

                    else if (segmentation == Utils.Constants.Segmentation.All)
                    {
                        var union = results.Union(poolLeads);
                        var joinedCount = union.Count();

                        results = union;
                    }
                }

                // Dates
                if (!(date1 == DateTime.MinValue && date2 == DateTime.MaxValue))
                    results = results.Where(l => l.LeadDate >= date1 && l.LeadDate <= date2);

                // Actual dashboard stats caluclation
                // =============================================================================================================================
                var leads = results.ToList();

                // Leads stats
                int leadsCount = leads.Count;
                List<int> leadsData = new List<int>();
                List<string> leadsDataLabels = new List<string>();

                DateTime minDate = leads.OrderBy(l => l.LeadDate).Select(l => l.LeadDate).Cast<DateTime>().FirstOrDefault();
                DateTime maxDate = leads.OrderByDescending(l => l.LeadDate).Select(l => l.LeadDate).Cast<DateTime>().FirstOrDefault();

                var days = (maxDate - minDate).TotalDays;
                var factor = days / 12;

                DateTime startDate = minDate;
                foreach (int d in Enumerable.Range(1, 12))
                {
                    DateTime endDate = startDate.AddDays(factor);
                    leadsData.Add(leads.Where(l => l.LeadDate >= startDate && l.LeadDate <= endDate).Count());
                    leadsDataLabels.Add(startDate.ToString("d MMM yyyy") + " - " + endDate.ToString("d MMM yyyy"));
                    startDate = startDate.AddDays(factor);
                }

                // Sales opportunity
                var salesOpportunity = leads.Count * campaignsList.Sum(c => c.ServiceCost);

                // New Leads
                var newLeads = leads.OrderByDescending(l => l.LeadDate).Take(3);

                // Average lead score
                var averageLeadScore = 0.0;
                leads.ToList().ForEach(l => averageLeadScore += l.LeadScore);
                averageLeadScore = averageLeadScore / leads.Count;
                if (double.IsNaN(averageLeadScore)) { averageLeadScore = 0.0; }

                // Average lead cost
                //var averageLeadCost = salesOpportunity / leads.Count
                var averageLeadCost = campaignsList.Sum(c => c.AdminBudget + c.MediaBudget) / leads.Count;

                // Hot leads
                var hotLeads = leads.Count(l => l.LeadDate >= DateTime.Now.AddDays(-5));

                // First aproach call done
                var firstAproachCallDone = leads.Count(l => l.Called);

                // % Total calls
                var totalCalls = (leads.Count <= 0) ? 0 : (firstAproachCallDone * 100) / leads.Count;

                // Upsell potential
                var upsellPotential = leads.Count(l => l.UpsellPotential);

                // Industry alert
                var industryAlert = leads.Count(l => l.AttractiveIndustry);

                // High urgency
                var highUrgency = leads.Count(l => l.TimeOpportunity);

                // Destination authority
                var destinationAuthority = leads.Count(l => l.JobAuthority);

                // All star
                var allStar = leads.Where(l => l.AllStar).Count();

                return new
                {
                    LeadsCount = leadsCount,
                    LeadsData = leadsData.ToArray(),
                    LeadsDataLabels = leadsDataLabels.ToArray(),
                    NewLeads = newLeads,
                    AverageLeadScore = averageLeadScore,
                    FirstAproachCallDone = firstAproachCallDone,
                    TotalCalls = totalCalls,
                    UpsellPotential = upsellPotential,
                    IndustryAlert = industryAlert,
                    DestinationAuthority = destinationAuthority,
                    AllStar = allStar,
                    HighUrgency = highUrgency,
                    HotLeads = hotLeads,
                    SalesOpportunity = salesOpportunity,
                    AverageLeadCost = averageLeadCost
                };
            }
        }

        // Private methods
        // =====================================================================================
        private List<long?> getAllUserCampaignsIds()
        {
            using (var context = new ApplicationDbContext())
            {
                var user = UserManager(context).FindById(User.Identity.GetUserId());
                var campaigns = context.Campaigns
                                    .Where(c => c.UserId == user.Id)
                                    .Select(c => c.Id)
                                    .Cast<long?>()
                                    .ToList();
                return campaigns;
            }
        }

        private UserManager<ApplicationUser> UserManager(ApplicationDbContext context)
        {
            return new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
        }

        private DateTime parseJSDate(string date)
        {
            return DateTime.ParseExact(date.Substring(0, 24),
                              "ddd MMM d yyyy HH:mm:ss",
                              CultureInfo.InvariantCulture);
        }

        // POST api/<controller>
        public void Post([FromBody]string value)
        {
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}