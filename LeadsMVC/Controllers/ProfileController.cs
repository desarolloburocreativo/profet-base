﻿using LeadsMVC.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LeadsMVC.Controllers
{
    public class ProfileController : Controller
    {
        // GET: Profile
        public ActionResult Index()
        {
            using (var context = new ApplicationDbContext())
            {
                var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
                var user = userManager.FindById(User.Identity.GetUserId());
                var campaigns = context.Campaigns
                                    .Where(c => c.UserId == user.Id)
                                    .ToList();

                // Set leads count
                //campaigns.ForEach(c =>
                //{
                //    c.LeadsCount = LeadsCountForCampaign(c.Id, context);
                //});

                var industries = context.Industries
                                        .Where(i => i.Users.Any(u => u.Id == user.Id))
                                        .Select(i => i.Name)
                                        .ToList();

                return View(new UserCampaignsIndustries(campaigns, user, industries));
            }
        }

        private double LeadsCountForCampaign(long id, ApplicationDbContext context)
        {
            return context.Leads.Where(l => l.CampaignId == id).Count();
        }
    }
}