﻿using LeadsMVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace LeadsMVC.Controllers
{
    public class ValuesController1 : ApiController
    {
        // GET api/<controller>
        public IEnumerable<Lead> Get()
        {
            using (var context = new ApplicationDbContext())
            {
                // Query for all blogs with names starting with B 
                var leads = context.Leads.SqlQuery("SELECT * FROM dbo.Leads").ToList();
                return leads;
            }
        }

        // GET api/<controller>/5
        public Lead Get(int id)
        {
            using (var context = new ApplicationDbContext())
            {
                var lead = from b in context.Leads
                           where b.Id.Equals(id)
                           select b;

                return lead.FirstOrDefault();
            }
        }

        // POST api/<controller>
        public void Post([FromBody]string value)
        {
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}