﻿using LeadsMVC.Models;
using LeadsMVC.Utils;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace LeadsMVC.Controllers
{
    public class ReportsController : Controller
    {
        // GET: Reports
        [Authorize]
        public ActionResult Index()
        {
            using (var context = new ApplicationDbContext())
            {
                var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
                var user = userManager.FindById(User.Identity.GetUserId());

                var reports = context.Reports
                                    .Where(c => c.UserId == user.Id)
                                    .ToList();
                return View(reports);
            }
        }

        // GET: Reports/Admin
        [Authorize(Roles = "Admin, Dev")]
        public ActionResult Admin()
        {
            using (var context = new ApplicationDbContext())
            {
                return View();
            }
        }

        [HttpPost]
        [Authorize(Roles = "Admin, Dev")]
        public ActionResult UploadFiles(string userId)
        {
            bool success = true;
            string fileName = String.Empty;
            try
            {
                foreach (string fileKey in Request.Files)
                {
                    HttpPostedFileBase file = Request.Files[fileKey];
                    fileName = file.FileName;
                    if (file != null && file.ContentLength > 0)
                    {
                        // Store file
                        using (var context = new ApplicationDbContext())
                        {
                            var originalDirectory = new DirectoryInfo(string.Format("{0}Uploads\\Reports\\" + userId, Server.MapPath(@"\")));
                            string path = Path.Combine(originalDirectory.ToString(), "");

                            // Save to directory
                            if (!Directory.Exists(path))
                                Directory.CreateDirectory(path);

                            // Same filename exists in server
                            int i = 1;
                            path = string.Format("{0}\\{1}", path, file.FileName);
                            while (System.IO.File.Exists(path))
                            {
                                var maybeFileName = String.Format("{0}({1}){2}",
                                         Path.GetFileNameWithoutExtension(path), i, Path.GetExtension(path));
                                path = Path.Combine(Path.GetDirectoryName(path), maybeFileName);
                                i++;
                            }
                            file.SaveAs(path);

                            context.Reports.AddOrUpdate(new Report() { FileName = fileName, Path = path, UserId = userId, UploadedDate = DateTime.Now });
                            context.SaveChanges();
                        }
                    }
                }

            }
            catch (Exception ex) { success = false; }

            if (success) { return Json(new { Message = "Success" }, JsonRequestBehavior.AllowGet); }
            else { HttpContext.Response.StatusCode = (int)HttpStatusCode.BadRequest; return Json(new { Message = "Error in saving file" }, JsonRequestBehavior.AllowGet); }
        }

        [Authorize]
        public ActionResult Download(long reportId)
        {
            using (var context = new ApplicationDbContext())
            {
                if (ReportBelongsToUser(reportId))
                {
                    string file = context.Reports.Where(r => r.Id == reportId).Select(r => r.Path).FirstOrDefault();
                    string fileName = context.Reports.Where(r => r.Id == reportId).Select(r => r.FileName).FirstOrDefault();

                    if (System.IO.File.Exists(file))
                    {
                        return File(file, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
                    }
                    else
                    {
                        HttpContext.Response.StatusCode = (int)HttpStatusCode.BadRequest;
                        return Json(new { Message = "File not in server" }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    return Json(new { Message = "File does not belongs to you" }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        private bool ReportBelongsToUser(long reportId)
        {
            using (var context = new ApplicationDbContext())
            {
                var user = UserManager(context).FindById(User.Identity.GetUserId());

                // If admin then yes it belongs to him lol
                if (UserManager(context).IsInRole(user.Id, "Admin"))
                    return true;

                var reports = context.Reports.Where(r => r.UserId == user.Id).Select(r => r.Id).ToList();
                return reports.Contains(reportId);
            }
        }

        private UserManager<ApplicationUser> UserManager(ApplicationDbContext context)
        {
            return new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
        }

        [HttpGet]
        [Authorize(Roles = "Admin, Dev")]
        public JsonCamelCaseResult GetReports(string userId)
        {
            using (var context = new ApplicationDbContext())
            {
                var reports = context.Reports
                                    .Where(c => c.UserId == userId)
                                    .ToList();

                return new JsonCamelCaseResult(reports, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPut]
        [Authorize(Roles = "Admin, Dev")]
        public void EditReport(long id, [System.Web.Http.FromBody]Report report)
        {
            if (String.IsNullOrWhiteSpace(report.FileName))
            {
                Response.StatusCode = 400;
                return;
            }

            using (var context = new ApplicationDbContext())
            {
                context.Reports.Attach(report);
                context.Entry(report).Property(x => x.FileName).IsModified = true;
                context.SaveChanges();
            }
        }

        [HttpDelete]
        [Authorize(Roles = "Admin, Dev")]
        public void DeleteReport(long id)
        {
            using (var context = new ApplicationDbContext())
            {
                string file = context.Reports.Where(r => r.Id == id).Select(r => r.Path).FirstOrDefault();
                if (System.IO.File.Exists(file))
                {
                    System.IO.File.Delete(file);
                }

                // Delete from db
                var report = new Report { Id = id };
                context.Reports.Attach(report);
                context.Reports.Remove(report);
                context.SaveChanges();

                return;
            }
        }
    }
}