﻿using LeadsMVC.Models;
using LeadsMVC.Utils;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace LeadsMVC.Controllers
{
    [Authorize]
    public class LeadsController : Controller
    {
        // GET: Leads
        public ActionResult Index()
        {
            return View();
        }

        // GET: Leads
        [Authorize(Roles = "Admin, Dev")]
        public ActionResult Admin()
        {
            return View();
        }

        [Authorize(Roles = "Admin, Dev")]
        public ActionResult LeadsAdmin()
        {
            return View();
        }
    }
}