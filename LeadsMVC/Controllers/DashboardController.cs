﻿using LeadsMVC.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net.Http;
using LeadsMVC.Utils;

namespace LeadsMVC.Controllers
{
    [Authorize]
    public class DashboardController : Controller
    {
        // GET: Dashboard
        public ActionResult Index()
        {
            return View();
        }

        [Authorize(Roles = "Admin, Dev")]
        public ActionResult Admin()
        {
            return View();
        }

        [Authorize(Roles = "Admin, Dev")]
        public ActionResult Users()
        {
            return View();
        }

        [HttpPost]
        [Authorize(Roles = "Admin, Dev")]
        public ActionResult MakeIndustries([System.Web.Http.FromBody]string industries)
        {
            if (industries.Length > 0)
            {
                using (var context = new ApplicationDbContext())
                {
                    var industriesList = new List<Industry>();
                    foreach (string industryName in industries.Split(','))
                        industriesList.Add(new Industry() { Name = industryName, Users = new List<ApplicationUser>() });

                    context.Industries.AddRange(industriesList);
                    context.SaveChanges();

                    return Json(new { success = true, responseText = "Industries saved" }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                Response.StatusCode = 400;
                return Json(new { success = false, responseText = "Must have at least one industry" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPut]
        [Authorize(Roles = "Admin, Dev")]
        public void EditIndustry(int id, [System.Web.Http.FromBody]Industry industry)
        {
            if (String.IsNullOrWhiteSpace(industry.Name))
            {
                Response.StatusCode = 400;
                return;
            }

            using (var context = new ApplicationDbContext())
            {
                context.Industries.Attach(industry);
                context.Entry(industry).Property(x => x.Name).IsModified = true;
                context.Entry(industry).Property(x => x.Description).IsModified = true;
                context.SaveChanges();
            }
        }

        [HttpDelete]
        [Authorize(Roles = "Admin, Dev")]
        public void DeleteIndustry(long id)
        {
            using (var context = new ApplicationDbContext())
            {
                var industry = context.Industries.SingleOrDefault(i => i.Id == id); //returns a single item.
                if (industry != null)
                {
                    context.Industries.Remove(industry);
                    context.SaveChanges();
                }
                else
                {
                    Response.StatusCode = 400;
                    return;
                }
            }
        }

        [HttpGet]
        [Authorize(Roles = "Admin, Dev")]
        public JsonResult GetIndustries()
        {
            using (var context = new ApplicationDbContext())
            {
                var industries = from industry in context.Industries
                                 select new { id = industry.Id, name = industry.Name, description = industry.Description };

                return Json(industries.ToList(), JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        [Authorize(Roles = "Admin, Dev")]
        public JsonCamelCaseResult GetUsersIndustries()
        {
            using (var context = new ApplicationDbContext())
            {
                var userIndustries = context.Database.SqlQuery<UsersIndustries>(
                       @"SELECT u.Id AS UserID, u.UserName, CONCAT(u.FirstName, ' ', u.LastName) AS UserFullName, i.Name AS IndustryName, i.Id AS IndustryId 
                        FROM UserIndustry ui
                        JOIN Users u on ui.UserId = u.Id
                        JOIN Industries i on ui.IndustryId = i.Id").ToList();

                return new JsonCamelCaseResult(userIndustries, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpDelete]
        [Authorize(Roles = "Admin, Dev")]
        public void DeleteUserIndustry(string userId, long industryId)
        {
            using (var context = new ApplicationDbContext())
            {
                context.Database.ExecuteSqlCommand("DELETE FROM UserIndustry WHERE UserId = @p0 AND IndustryId = @p1", userId, industryId);
            }
        }

        [HttpPost]
        [Authorize(Roles = "Admin, Dev")]
        public ActionResult MakeRelation(string userId, string[] industries)
        {
            if (industries.Count() > 0)
            {
                using (var context = new ApplicationDbContext())
                {
                    // Get user
                    var user = context.Users.Where(i => i.Id == userId).FirstOrDefault();

                    foreach (long industryId in Array.ConvertAll(industries, long.Parse))
                    {
                        // Get industry
                        var industry = context.Industries.Where(i => i.Id == industryId).FirstOrDefault();
                        if (industry == null)
                        {
                            Response.StatusCode = 400;
                            return Json(new { success = false, responseText = "Null industry" }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            var currentIndustryUsersId = getUsersIdOfIndustry(industry);

                            // If user not already in the industry
                            if (!currentIndustryUsersId.Contains(userId))
                            {
                                industry.Users.Add(user);
                                context.SaveChanges();
                            }
                        }
                    }

                    return Json(new { success = true, responseText = "Industries saved" }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                Response.StatusCode = 400;
                return Json(new { success = false, responseText = "Must have at least one industry" }, JsonRequestBehavior.AllowGet);
            }
        }

        private List<string> getUsersIdOfIndustry(Industry industry)
        {
            using (var context = new ApplicationDbContext())
            {
                return context.Users
                    .Where(u => u.Industries.Any(i => i.Id == industry.Id))
                    .Select(i => i.Id)
                    .ToList();
            }
        }

        // GET api/<controller>
        [Authorize(Roles = "Admin, Dev")]
        public JsonCamelCaseResult GetUsersWithRoles()
        {
            using (var context = new ApplicationDbContext())
            {
                var query = context.Database.SqlQuery<UserRoles>(
                       @"SELECT u.Id AS UserID, u.UserName, u.FirstName AS UserFirstName, u.LastName AS UserLastName, 
                        u.CompanyName, u.Phone, u.Address, u.Web, u.Address, u.Contact, u.PhoneExt, u.Mobile, u.IndustrySector,
                        r.Name AS RoleName, r.Id AS RoleId 
                        FROM UserRoles ur
                        JOIN Users u on ur.UserId = u.Id
                        JOIN Roles r on ur.RoleId = r.Id");

                return new JsonCamelCaseResult(query.ToList(), JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPut]
        [Authorize(Roles = "Admin, Dev")]
        public void EditUser(string id, [System.Web.Http.FromBody]ApplicationUser user)
        {
            if (String.IsNullOrWhiteSpace(user.UserName))
            {
                Response.StatusCode = 400;
                return;
            }

            using (var context = new ApplicationDbContext())
            {
                context.Users.Attach(user);
                context.Entry(user).Property(x => x.UserName).IsModified = true;
                context.Entry(user).Property(x => x.FirstName).IsModified = true;
                context.Entry(user).Property(x => x.LastName).IsModified = true;

                context.Entry(user).Property(x => x.CompanyName).IsModified = true;
                context.Entry(user).Property(x => x.IndustrySector).IsModified = true;
                context.Entry(user).Property(x => x.Phone).IsModified = true;
                context.Entry(user).Property(x => x.PhoneExt).IsModified = true;
                context.Entry(user).Property(x => x.Mobile).IsModified = true;
                context.Entry(user).Property(x => x.Address).IsModified = true;
                context.Entry(user).Property(x => x.Web).IsModified = true;
                context.Entry(user).Property(x => x.Contact).IsModified = true;
                context.SaveChanges();
            }
        }

        [HttpDelete]
        [Authorize(Roles = "Admin, Dev")]
        public void DeleteUser(string id)
        {
            using (var context = new ApplicationDbContext())
            {
                var user = context.Users.SingleOrDefault(u => u.Id == id); //returns a single item.
                if (user != null)
                {
                    context.Users.Remove(user);
                    context.SaveChanges();
                    return;
                }
                else
                {
                    Response.StatusCode = 400;
                    return;
                }
            }
        }
    }
}