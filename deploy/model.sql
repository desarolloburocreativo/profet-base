CREATE TABLE [dbo].[__MigrationHistory] (
    [MigrationId]    NVARCHAR (150)  NOT NULL,
    [ContextKey]     NVARCHAR (300)  NOT NULL,
    [Model]          VARBINARY (MAX) NOT NULL,
    [ProductVersion] NVARCHAR (32)   NOT NULL
);

GO
CREATE TABLE [dbo].[CalendarEvents] (
    [Id]          BIGINT         IDENTITY (1, 1) NOT NULL,
    [Title]       NVARCHAR (MAX) NULL,
    [Start]       DATETIME       NOT NULL,
    [End]         DATETIME       NOT NULL,
    [Color]       NVARCHAR (MAX) NULL,
    [Description] NVARCHAR (MAX) NULL,
    [UserId]      NVARCHAR (128) NULL
);

GO
CREATE TABLE [dbo].[Campaigns] (
    [Id]               BIGINT         IDENTITY (1, 1) NOT NULL,
    [Description]      NVARCHAR (MAX) NULL,
    [Name]             NVARCHAR (MAX) NULL,
    [UserId]           NVARCHAR (128) NULL,
    [ServiceCost]      FLOAT (53)     DEFAULT ((0)) NOT NULL,
    [Impressions]      FLOAT (53)     DEFAULT ((0)) NOT NULL,
    [Clicks]           FLOAT (53)     DEFAULT ((0)) NOT NULL,
    [Engagement]       FLOAT (53)     DEFAULT ((0)) NOT NULL,
    [Community]        FLOAT (53)     DEFAULT ((0)) NOT NULL,
    [AdminBudget]      FLOAT (53)     DEFAULT ((0)) NOT NULL,
    [MediaBudget]      FLOAT (53)     DEFAULT ((0)) NOT NULL,
    [SalesRep]         NVARCHAR (MAX) NULL,
    [AccountMgmt]      NVARCHAR (MAX) NULL,
    [ContractUrl]      NVARCHAR (MAX) NULL,
    [ConfigurationUrl] NVARCHAR (MAX) NULL,
    [StatmentUrl]      NVARCHAR (MAX) NULL
);

GO
CREATE TABLE [dbo].[Industries] (
    [Id]          BIGINT         IDENTITY (1, 1) NOT NULL,
    [Name]        NVARCHAR (MAX) NULL,
    [Description] NVARCHAR (MAX) NULL
);

GO
CREATE TABLE [dbo].[Leads] (
    [Id]                        BIGINT         IDENTITY (1, 1) NOT NULL,
    [Name]                      NVARCHAR (MAX) NULL,
    [Email]                     NVARCHAR (MAX) NULL,
    [Phone]                     NVARCHAR (MAX) NULL,
    [Comments]                  NVARCHAR (MAX) NULL,
    [Genre]                     NVARCHAR (MAX) NULL,
    [CivilStatus]               NVARCHAR (MAX) NULL,
    [Age]                       NVARCHAR (MAX) NULL,
    [Birthday]                  NVARCHAR (MAX) NULL,
    [Colony]                    NVARCHAR (MAX) NULL,
    [City]                      NVARCHAR (MAX) NULL,
    [State]                     NVARCHAR (MAX) NULL,
    [CP]                        NVARCHAR (MAX) NULL,
    [NSE]                       NVARCHAR (MAX) NULL,
    [Position]                  NVARCHAR (MAX) NULL,
    [Company]                   NVARCHAR (MAX) NULL,
    [CompanyType]               NVARCHAR (MAX) NULL,
    [IndistrySector]            NVARCHAR (MAX) NULL,
    [Employees]                 INT            NOT NULL,
    [Interests]                 NVARCHAR (MAX) NULL,
    [Preferences]               NVARCHAR (MAX) NULL,
    [Activities]                NVARCHAR (MAX) NULL,
    [PracticeSports]            NVARCHAR (MAX) NULL,
    [Habits]                    NVARCHAR (MAX) NULL,
    [Values]                    NVARCHAR (MAX) NULL,
    [Believes]                  NVARCHAR (MAX) NULL,
    [Emotions]                  NVARCHAR (MAX) NULL,
    [BehaviorTags]              NVARCHAR (MAX) NULL,
    [BuyingTime]                FLOAT (53)     NOT NULL,
    [BuyingDecision]            FLOAT (53)     NOT NULL,
    [NeedDegree]                FLOAT (53)     NOT NULL,
    [ComparingAgainst]          FLOAT (53)     NOT NULL,
    [AuthorityTags]             NVARCHAR (MAX) NULL,
    [LeadDate]                  DATETIME       NOT NULL,
    [InterestedIn]              NVARCHAR (MAX) NULL,
    [MessageSent]               NVARCHAR (MAX) NULL,
    [OpportunityValue]          NVARCHAR (MAX) NULL,
    [ProspectSource]            NVARCHAR (MAX) NULL,
    [ProsepectGeneratorCompany] NVARCHAR (MAX) NULL,
    [InterestTags]              NVARCHAR (MAX) NULL,
    [Budget]                    FLOAT (53)     NOT NULL,
    [Job]                       FLOAT (53)     NOT NULL,
    [Authority]                 FLOAT (53)     NOT NULL,
    [Need]                      FLOAT (53)     NOT NULL,
    [TimeFrame]                 FLOAT (53)     NOT NULL,
    [Type]                      FLOAT (53)     NOT NULL,
    [Industry]                  FLOAT (53)     NOT NULL,
    [Status]                    FLOAT (53)     NOT NULL,
    [Contact]                   FLOAT (53)     NOT NULL,
    [QualityScore]              FLOAT (53)     NOT NULL,
    [Aging]                     FLOAT (53)     NOT NULL,
    [Engagement]                FLOAT (53)     NOT NULL,
    [UpsellPotential]           BIT            NOT NULL,
    [LeadScore]                 FLOAT (53)     NOT NULL,
    [ClosedSell]                NVARCHAR (MAX) NULL,
    [SellApproxAmount]          FLOAT (53)     NOT NULL,
    [CampaignName]              NVARCHAR (MAX) NULL,
    [CampaignId]                BIGINT         NOT NULL,
    [Notes]                     NVARCHAR (MAX) NULL,
    [Called]                    BIT            DEFAULT ((0)) NOT NULL,
    [StreetAndNumber]           NVARCHAR (MAX) NULL,
    [Generals]                  FLOAT (53)     NOT NULL
);

GO
CREATE TABLE [dbo].[Reports] (
    [Id]           BIGINT         IDENTITY (1, 1) NOT NULL,
    [FileName]     NVARCHAR (MAX) NULL,
    [Path]         NVARCHAR (MAX) NULL,
    [UserId]       NVARCHAR (128) NULL,
    [UploadedDate] DATETIME       DEFAULT ('1900-01-01T00:00:00.000') NOT NULL
);

GO
CREATE TABLE [dbo].[Roles] (
    [Id]   NVARCHAR (128) NOT NULL,
    [Name] NVARCHAR (256) NOT NULL
);

GO
CREATE TABLE [dbo].[UserClaims] (
    [Id]         INT            IDENTITY (1, 1) NOT NULL,
    [UserId]     NVARCHAR (128) NOT NULL,
    [ClaimType]  NVARCHAR (MAX) NULL,
    [ClaimValue] NVARCHAR (MAX) NULL
);

GO
CREATE TABLE [dbo].[UserIndustry] (
    [UserId]     NVARCHAR (128) NOT NULL,
    [IndustryId] BIGINT         NOT NULL
);

GO
CREATE TABLE [dbo].[UserLogins] (
    [LoginProvider] NVARCHAR (128) NOT NULL,
    [ProviderKey]   NVARCHAR (128) NOT NULL,
    [UserId]        NVARCHAR (128) NOT NULL
);

GO
CREATE TABLE [dbo].[UserRoles] (
    [UserId] NVARCHAR (128) NOT NULL,
    [RoleId] NVARCHAR (128) NOT NULL
);

GO
CREATE TABLE [dbo].[Users] (
    [Id]                   NVARCHAR (128) NOT NULL,
    [Email]                NVARCHAR (256) NULL,
    [EmailConfirmed]       BIT            NOT NULL,
    [PasswordHash]         NVARCHAR (MAX) NULL,
    [SecurityStamp]        NVARCHAR (MAX) NULL,
    [PhoneNumber]          NVARCHAR (MAX) NULL,
    [PhoneNumberConfirmed] BIT            NOT NULL,
    [TwoFactorEnabled]     BIT            NOT NULL,
    [LockoutEndDateUtc]    DATETIME       NULL,
    [LockoutEnabled]       BIT            NOT NULL,
    [AccessFailedCount]    INT            NOT NULL,
    [UserName]             NVARCHAR (256) NOT NULL,
    [FirstName]            NVARCHAR (MAX) NULL,
    [LastName]             NVARCHAR (MAX) NULL,
    [CompanyName]          NVARCHAR (MAX) NULL,
    [Phone]                NVARCHAR (MAX) NULL,
    [Address]              NVARCHAR (MAX) NULL,
    [Web]                  NVARCHAR (MAX) NULL,
    [Contact]              NVARCHAR (MAX) NULL,
    [PhoneExt]             NVARCHAR (MAX) NULL,
    [Mobile]               NVARCHAR (MAX) NULL
);

GO
ALTER TABLE [dbo].[UserClaims]
    ADD CONSTRAINT [FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId] FOREIGN KEY ([UserId]) REFERENCES [dbo].[Users] ([Id]) ON DELETE CASCADE;

GO
ALTER TABLE [dbo].[UserLogins]
    ADD CONSTRAINT [FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId] FOREIGN KEY ([UserId]) REFERENCES [dbo].[Users] ([Id]) ON DELETE CASCADE;

GO
ALTER TABLE [dbo].[UserRoles]
    ADD CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId] FOREIGN KEY ([RoleId]) REFERENCES [dbo].[Roles] ([Id]) ON DELETE CASCADE;

GO
ALTER TABLE [dbo].[UserRoles]
    ADD CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId] FOREIGN KEY ([UserId]) REFERENCES [dbo].[Users] ([Id]) ON DELETE CASCADE;

GO
ALTER TABLE [dbo].[CalendarEvents]
    ADD CONSTRAINT [FK_dbo.CalendarEvents_dbo.Users_UserId] FOREIGN KEY ([UserId]) REFERENCES [dbo].[Users] ([Id]);

GO
ALTER TABLE [dbo].[Campaigns]
    ADD CONSTRAINT [FK_dbo.Campaigns_dbo.Users_User_Id] FOREIGN KEY ([UserId]) REFERENCES [dbo].[Users] ([Id]);

GO
ALTER TABLE [dbo].[Leads]
    ADD CONSTRAINT [FK_dbo.Leads_dbo.Campaigns_Campaign_Id] FOREIGN KEY ([CampaignId]) REFERENCES [dbo].[Campaigns] ([Id]);

GO
ALTER TABLE [dbo].[Leads]
    ADD CONSTRAINT [FK_dbo.Leads_dbo.Campaigns_CampaignId] FOREIGN KEY ([CampaignId]) REFERENCES [dbo].[Campaigns] ([Id]) ON DELETE CASCADE;

GO
ALTER TABLE [dbo].[Reports]
    ADD CONSTRAINT [FK_dbo.Reports_dbo.Users_UserId] FOREIGN KEY ([UserId]) REFERENCES [dbo].[Users] ([Id]);

GO
ALTER TABLE [dbo].[UserIndustry]
    ADD CONSTRAINT [FK_dbo.UserIndusty_dbo.Industries_Industry_Id] FOREIGN KEY ([IndustryId]) REFERENCES [dbo].[Industries] ([Id]) ON DELETE CASCADE;

GO
ALTER TABLE [dbo].[UserIndustry]
    ADD CONSTRAINT [FK_dbo.UserIndusty_dbo.Users_ApplicationUser_Id] FOREIGN KEY ([UserId]) REFERENCES [dbo].[Users] ([Id]) ON DELETE CASCADE;

GO
ALTER TABLE [dbo].[Leads]
    ADD CONSTRAINT [DF_dbo.Leads_Aging] DEFAULT ((0)) FOR [Aging];

GO
ALTER TABLE [dbo].[Leads]
    ADD CONSTRAINT [DF_dbo.Leads_Authority] DEFAULT ((0)) FOR [Authority];

GO
ALTER TABLE [dbo].[Leads]
    ADD CONSTRAINT [DF_dbo.Leads_Budget] DEFAULT ((0)) FOR [Budget];

GO
ALTER TABLE [dbo].[Leads]
    ADD CONSTRAINT [DF_dbo.Leads_BuyingDecision] DEFAULT ((0)) FOR [BuyingDecision];

GO
ALTER TABLE [dbo].[Leads]
    ADD CONSTRAINT [DF_dbo.Leads_BuyingTime] DEFAULT ((0)) FOR [BuyingTime];

GO
ALTER TABLE [dbo].[Leads]
    ADD CONSTRAINT [DF_dbo.Leads_ComparingAgainst] DEFAULT ((0)) FOR [ComparingAgainst];

GO
ALTER TABLE [dbo].[Leads]
    ADD CONSTRAINT [DF_dbo.Leads_Contact] DEFAULT ((0)) FOR [Contact];

GO
ALTER TABLE [dbo].[Leads]
    ADD CONSTRAINT [DF_dbo.Leads_Engagement] DEFAULT ((0)) FOR [Engagement];

GO
ALTER TABLE [dbo].[Leads]
    ADD CONSTRAINT [DF_dbo.Leads_Generals] DEFAULT ((0)) FOR [Generals];

GO
ALTER TABLE [dbo].[Leads]
    ADD CONSTRAINT [DF_dbo.Leads_Industry] DEFAULT ((0)) FOR [Industry];

GO
ALTER TABLE [dbo].[Leads]
    ADD CONSTRAINT [DF_dbo.Leads_Job] DEFAULT ((0)) FOR [Job];

GO
ALTER TABLE [dbo].[Leads]
    ADD CONSTRAINT [DF_dbo.Leads_LeadScore] DEFAULT ((0)) FOR [LeadScore];

GO
ALTER TABLE [dbo].[Leads]
    ADD CONSTRAINT [DF_dbo.Leads_Need] DEFAULT ((0)) FOR [Need];

GO
ALTER TABLE [dbo].[Leads]
    ADD CONSTRAINT [DF_dbo.Leads_NeedDegree] DEFAULT ((0)) FOR [NeedDegree];

GO
ALTER TABLE [dbo].[Leads]
    ADD CONSTRAINT [DF_dbo.Leads_QualityScore] DEFAULT ((0)) FOR [QualityScore];

GO
ALTER TABLE [dbo].[Leads]
    ADD CONSTRAINT [DF_dbo.Leads_Status] DEFAULT ((0)) FOR [Status];

GO
ALTER TABLE [dbo].[Leads]
    ADD CONSTRAINT [DF_dbo.Leads_TimeFrame] DEFAULT ((0)) FOR [TimeFrame];

GO
ALTER TABLE [dbo].[Leads]
    ADD CONSTRAINT [DF_dbo.Leads_Type] DEFAULT ((0)) FOR [Type];

GO
ALTER TABLE [dbo].[__MigrationHistory]
    ADD CONSTRAINT [PK_dbo.__MigrationHistory] PRIMARY KEY CLUSTERED ([MigrationId] ASC, [ContextKey] ASC);

GO
ALTER TABLE [dbo].[CalendarEvents]
    ADD CONSTRAINT [PK_dbo.CalendarEvents] PRIMARY KEY CLUSTERED ([Id] ASC);

GO
ALTER TABLE [dbo].[Campaigns]
    ADD CONSTRAINT [PK_dbo.Campaigns] PRIMARY KEY CLUSTERED ([Id] ASC);

GO
ALTER TABLE [dbo].[Industries]
    ADD CONSTRAINT [PK_dbo.Industries] PRIMARY KEY CLUSTERED ([Id] ASC);

GO
ALTER TABLE [dbo].[Leads]
    ADD CONSTRAINT [PK_dbo.Leads] PRIMARY KEY CLUSTERED ([Id] ASC);

GO
ALTER TABLE [dbo].[Reports]
    ADD CONSTRAINT [PK_dbo.Reports] PRIMARY KEY CLUSTERED ([Id] ASC);

GO
ALTER TABLE [dbo].[Roles]
    ADD CONSTRAINT [PK_dbo.Roles] PRIMARY KEY CLUSTERED ([Id] ASC);

GO
ALTER TABLE [dbo].[UserClaims]
    ADD CONSTRAINT [PK_dbo.UserClaims] PRIMARY KEY CLUSTERED ([Id] ASC);

GO
ALTER TABLE [dbo].[UserIndustry]
    ADD CONSTRAINT [PK_dbo.UserIndustry] PRIMARY KEY CLUSTERED ([UserId] ASC, [IndustryId] ASC);

GO
ALTER TABLE [dbo].[UserLogins]
    ADD CONSTRAINT [PK_dbo.UserLogins] PRIMARY KEY CLUSTERED ([LoginProvider] ASC, [ProviderKey] ASC, [UserId] ASC);

GO
ALTER TABLE [dbo].[UserRoles]
    ADD CONSTRAINT [PK_dbo.UserRoles] PRIMARY KEY CLUSTERED ([UserId] ASC, [RoleId] ASC);

GO
ALTER TABLE [dbo].[Users]
    ADD CONSTRAINT [PK_dbo.Users] PRIMARY KEY CLUSTERED ([Id] ASC);

GO
CREATE NONCLUSTERED INDEX [IX_UserId]
    ON [dbo].[CalendarEvents]([UserId] ASC);

GO
CREATE NONCLUSTERED INDEX [IX_UserId]
    ON [dbo].[Campaigns]([UserId] ASC);

GO
CREATE NONCLUSTERED INDEX [IX_CampaignId]
    ON [dbo].[Leads]([CampaignId] ASC);

GO
CREATE NONCLUSTERED INDEX [IX_UserId]
    ON [dbo].[Reports]([UserId] ASC);

GO
CREATE UNIQUE NONCLUSTERED INDEX [RoleNameIndex]
    ON [dbo].[Roles]([Name] ASC);

GO
CREATE NONCLUSTERED INDEX [IX_UserId]
    ON [dbo].[UserClaims]([UserId] ASC);

GO
CREATE NONCLUSTERED INDEX [IX_IndustryId]
    ON [dbo].[UserIndustry]([IndustryId] ASC);

GO
CREATE NONCLUSTERED INDEX [IX_UserId]
    ON [dbo].[UserIndustry]([UserId] ASC);

GO
CREATE NONCLUSTERED INDEX [IX_UserId]
    ON [dbo].[UserLogins]([UserId] ASC);

GO
CREATE NONCLUSTERED INDEX [IX_RoleId]
    ON [dbo].[UserRoles]([RoleId] ASC);

GO
CREATE NONCLUSTERED INDEX [IX_UserId]
    ON [dbo].[UserRoles]([UserId] ASC);

GO
CREATE UNIQUE NONCLUSTERED INDEX [UserNameIndex]
    ON [dbo].[Users]([UserName] ASC);

GO
